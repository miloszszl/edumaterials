const chai = require('chai');
const assert = chai.assert;
const app = require("../app");
const chaiHttp = require('chai-http');
let should = chai.should();
chai.use(chaiHttp);

describe("Users", function () {
    this.timeout(15000);

    it('/POST login_local false', (done) => {
        let user = {
            username: "aaa",
            password: "bbb"
        }
        chai.request(app)
            .post('/users/login')
            .send(user)
            .end((err, res) => {
                should.not.exist(err);
                res.type.should.equal('application/json');
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('code').eql(0);
                res.body.should.have.property('resp');
                done();
            });
    });

    it('/POST login_local true', (done) => {
        let user = {
            username: "qwerty321",
            password: "Qq!111"
        }
        chai.request(app)
            .post('/users/login')
            .send(user)
            .end((err, res) => {
                should.not.exist(err);
                res.type.should.equal('application/json');
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('code').eql(1);
                done();
            });
    });

    it('/GET logout', (done) => {
        chai.request(app)
            .get('/users/logout')
            .end((err, res) => {
                should.not.exist(err);
                res.type.should.equal('text/html');
                res.should.have.status(200);
                done();
            });
    });

});