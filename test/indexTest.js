const chai = require('chai');
const assert = chai.assert;
const app = require("../app");
const chaiHttp = require('chai-http');
let should = chai.should();
chai.use(chaiHttp);

describe("Index", function () {

    it('/GET start_page', (done) => {
        chai.request(app)
            .get('/')
            .end((err, res) => {
                should.not.exist(err);
                res.type.should.equal('text/html');
                res.should.have.status(200);
                done();
            });
    });

    it('/GET home', (done) => {
        chai.request(app)
            .get('/start')
            .end((err, res) => {
                should.not.exist(err);
                res.type.should.equal('text/html');
                res.should.have.status(200);
                done();
            });
    });

    it('/GET search_results', (done) => {
        chai.request(app)
            .get('/search')
            .query({term: 'mil'}) 
            .end((err, res) => {
                should.not.exist(err);
                res.type.should.equal('text/html');
                res.should.have.status(200);
                done();
            });
    });

    it('/GET query not string', (done) => {
        chai.request(app)
            .get('/query')
            .query({term: 111}) 
            .end((err, res) => {
                should.not.exist(err);
                res.type.should.equal('text/html');
                res.should.have.status(200);
                res.text.should.be.eql('[]');
                done();
            });
    });

    it('/GET query string', (done) => {
        chai.request(app)
            .get('/query')
            .query({term: "mil"}) 
            .end((err, res) => {
                should.not.exist(err);
                res.type.should.equal('text/html');
                res.should.have.status(200);
                res.text.length.should.be.above(1);
                done();
            });
    });
    
});
