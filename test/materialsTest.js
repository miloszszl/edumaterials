const chai = require('chai');
const assert = chai.assert;
const app = require("../app");
const chaiHttp = require('chai-http');
let should = chai.should();
chai.use(chaiHttp);

describe("Materials", function () {
    this.timeout(15000);

    it('/GET upload', (done) => {
        chai.request(app)
            .get('/materials/upload')
            .end((err, res) => {
                should.not.exist(err);
                res.type.should.equal('text/html');
                res.should.have.status(200);
                done();
            });
    });

    it('/GET download false', (done) => {
        chai.request(app)
            .get('/materials/123/download')
            .end((err, res) => {
                should.exist(err);
                res.type.should.equal('text/html');
                res.should.have.status(400);
                done();
            });
    });

    it('/GET periods', (done) => {
        chai.request(app)
            .get('/materials/periods')
            .end((err, res) => {
                should.not.exist(err);
                res.type.should.equal('application/json');
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('code').eql(1);
                res.body.should.have.property('periods');
                done();
            });
    });

    it('/GET subjects_and_categories false', (done) => {
        chai.request(app)
            .get('/materials/upload/123/subjects_and_categories')
            .end((err, res) => {
                should.not.exist(err);
                res.type.should.equal('application/json');
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('code').eql(0);
                res.body.should.have.property('resp');
                done();
            });
    });

});