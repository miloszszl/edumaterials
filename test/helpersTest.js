const chai = require('chai');
const assert = chai.assert;
const UniversalHelper = require('../helpers/UniversalHelper');
const MetadataParser = require('../helpers/MetadataParser');
const SecurityHelper = require('../helpers/SecurityHelper');
var mongoose = require('mongoose');


describe("Helpers", function () {

    describe("UniversalHelper", function () {
        var objectIdArr = [];
        const size = 10;

        before(function () {
            for (var i = 0; i < size; i++) {
                objectIdArr.push(new mongoose.Types.ObjectId());
            }
        });


        it('stringifyIds', function () {

            const result = UniversalHelper.stringifyIds(objectIdArr);

            assert(Array.isArray(result));
            assert.equal(result.length, size);

            for (var i = 0; i < size; i++) {
                assert(typeof result[i] === 'string');
            }
        });

        it('checkIfElementIsInArray', function () {
            var arr = UniversalHelper.stringifyIds(objectIdArr);
            var obj = objectIdArr[size - 1];

            var result = UniversalHelper.checkIfElementIsInArray(arr, obj);
            assert.equal(result, true);
        });

        it('checkIfElementIsInArray', function () {
            const arr = UniversalHelper.stringifyIds(objectIdArr);
            var obj = -1;

            var result = UniversalHelper.checkIfElementIsInArray(arr, obj);
            assert.equal(result, false);
        });
    });


    describe("MatadataParser", function () {
        it('createExactString', function () {
            var result = MetadataParser.createExactString("abcd");
            assert.equal("^abcd$", result);
        });

        it('createBeginningString', function () {
            var result = MetadataParser.createBeginningString("abcd");
            assert.equal("^abcd", result);
        });

        it('createAcronymString', function () {
            var result = MetadataParser.createAcronymString("abcd");
            assert.equal('^a\\S*\\sb\\S*\\sc\\S*\\sd.*$', result);
        });

        it('createRegexObject', function () {
            const expected = {
                exact_name: "^abcd$",
                name_beginning: "^abcd",
                name_acronym: "^a\\S*\\sb\\S*\\sc\\S*\\sd.*$"
            }
            var result = MetadataParser.createRegexObject("abcd");
            assert(expected.exact_name == result.exact_name && expected.name_beginning == result.name_beginning && expected.name_acronym == result.name_acronym);
        });
    });

    describe("SecurityHelper", function () {
        it('xssClearNamesInArray', function () {
            var arr=[{name:'<script>alert("xss");</script>'},{name:"asd"},undefined,{name:'<script>alert("xss");</script>'},null,{name:undefined},{name:null},{name:'<script>alert("xss");</script>'}];
            SecurityHelper.xssClearNamesInArray(arr);
            var expected=[{name:'&lt;script&gt;alert("xss");&lt;/script&gt;'},{name:'asd'},undefined,{name:'&lt;script&gt;alert("xss");&lt;/script&gt;'},null,{name:undefined},{name:null},{name:'&lt;script&gt;alert("xss");&lt;/script&gt;'}];

            assert(arr.length==expected.length);
            for(var i=0;i<arr.length;i++){
                assert(typeof expected[i] === typeof arr[i]);
                if(expected[i] !=null && arr[i]!=null && typeof expected[i] !=='undefined' && typeof expected[i]!=='undefined')
                    assert(expected[i].name==arr[i].name);
            }
        });
    });

});