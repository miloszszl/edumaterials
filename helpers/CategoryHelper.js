var SecurityHelper=require('./SecurityHelper');

class CategoryHelper{
    static prepareCategoriesAutocomplete(categories){
        var my_arr=[];
        for(var i=0;i<categories.length;i++){
            my_arr.push(SecurityHelper.xssClearCategoryAutocomplete(categories[i]));
        }
        return my_arr;
    }
}

module.exports=CategoryHelper;