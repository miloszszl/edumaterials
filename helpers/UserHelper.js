
var UniversalHelper=require('./UniversalHelper');
var SecurityHelper=require('./SecurityHelper');

class UserHelper{
    static findFavouriteMaterialsIds(user){
        var favourite_materials_ids=[];
        
        if(Array.isArray(user.favourite_materials_ids)){
            favourite_materials_ids=UniversalHelper.stringifyIds(user.favourite_materials_ids);
        }

        return favourite_materials_ids;
    }

    static findLikedMaterialsIds(user){
        var liked_materials_ids=[];
        
        if(Array.isArray(user.liked_materials_ids)){
            liked_materials_ids=UniversalHelper.stringifyIds(user.liked_materials_ids);
        }
        
        return liked_materials_ids;
    }

    static prepareUsersForThumbanils(users){
        var thumbnail_users=[];
        var users_length=users.length;
        for(var i=0;i<users_length;i++){
            thumbnail_users.push(SecurityHelper.xssClearUserForThumbnail(users[i]));
        }
        return thumbnail_users;
    }

    static prepareUsersAutocomplete(users){
        var my_arr=[];
        var users_length=users.length;
        for(var i=0;i<users.length;i++){
            my_arr.push(SecurityHelper.xssClearUserAutocomplete(users[i]));
        }
        return my_arr;
    }

    static getLikedAndFavouriteMaterialsIds(req) {
        var favourite_materials_ids = [];
        var liked_materials_ids = [];
    
        if (req.isAuthenticated() && typeof req.user !== 'undefined' && Array.isArray(req.user.favourite_materials_ids) && Array.isArray(req.user.liked_materials_ids)) {
            favourite_materials_ids = UniversalHelper.stringifyIds(req.user.favourite_materials_ids);
            liked_materials_ids = UniversalHelper.stringifyIds(req.user.liked_materials_ids);
        }
        return { favourite_materials_ids: favourite_materials_ids, liked_materials_ids: liked_materials_ids };
    }
}

module.exports=UserHelper;