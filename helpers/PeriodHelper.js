var SecurityHelper=require('./SecurityHelper');

class PeriodHelper{
    static preparePeriodsAutocomplete(periods){
        var my_arr=[];
        var periods_length=periods.length;
        for(var i=0;i<periods.length;i++){
            my_arr.push(SecurityHelper.xssClearPeriodAutocomplete(periods[i]));
        }
        return my_arr;
    }
}

module.exports=PeriodHelper;