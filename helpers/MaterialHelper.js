var xss = require('xss');
var async = require('async');
var sanitize = require('mongo-sanitize');
var Period=require('../models/period');
var Subject=require('../models/subject');
var Category=require('../models/category');
var UniversalHelper=require('./UniversalHelper');
var SecurityHelper=require('./SecurityHelper');
var fs = require('fs');
var path = require('path');
var uniqid = require('uniqid');
var Material = require('../models/material')
var User = require('../models/user');
var mongoose = require('mongoose');

class MaterialHelper{

    static prepareMaterials(materials,favourite_materials_ids,liked_materials_ids,my_callback){

        var xss_clear_materials=[];
    
        async.eachSeries(materials,function(item,loop_callback){
            async.waterfall([
                function(callback){
                    var period_id=sanitize(item.period_id)
                    Period.findById(period_id,function(err,period){
                        if(err)
                            return callback(err);

                        if(period)
                            callback(null,xss(period.name));
                        else
                            callback(new Error("Wrong period."));
                    });
                },
                function(period_name,callback){
                    var subject_id=sanitize(item.subject_id);
                    Subject.findById(subject_id,function(err,subject){
                        if(err)
                            return callback(err);
                        
                        if(subject)
                            callback(null,period_name,xss(subject.name));
                        else
                            callback(new Error("Wrong subject!"));
                        
                    });
                },
                function(period_name,subject_name,callback){
                    var category_id=sanitize(item.category_id);
                    Category.findById(category_id,function(err,category){
                        if(err)
                            return callback(err);
                        
                        if(category)
                            callback(null,period_name,subject_name,xss(category.name));
                        else
                            callback(new Error("Wrong category!"));
                    });
                }
            ],function(err,period_name,subject_name,category_name){
                if(err)
                    return loop_callback(err);
                

                var is_favourite=UniversalHelper.checkIfElementIsInArray(favourite_materials_ids,item._id);
                var is_liked=UniversalHelper.checkIfElementIsInArray(liked_materials_ids,item._id);

                xss_clear_materials.push({
                    id:item._id,
                    name:xss(item.name),
                    type:xss(item.type),
                    extension:xss(item.extension),
                    is_favourite:is_favourite,
                    is_liked:is_liked,
                    period_name:period_name,
                    subject_name:subject_name,
                    category_name:category_name
                });
    
                loop_callback();
            });
        },
        function(err) {
            if(err)
                return my_callback(err);
            
            return my_callback(null,xss_clear_materials); 
        });
    }

    static prepareMaterialsAutocomplete(materials){
        var my_arr=[];
        var materials_length=materials.length;
        for(var i=0;i<materials.length;i++){
            my_arr.push(SecurityHelper.xssClearMaterialAutocomplete(materials[i]));
        }
        return my_arr;
    }

    static calculateUnits(material){
        var units=0;
        //calculating size . By default size is in bytes [B]
        for(var j=0;j<4;j++){
            if(material.size>1024){
                material.size=material.size/1024;
                units++;
            }
        }
    
        switch(units){
            case 0:
                units="B";
                break;
            case 1:
                units="KB";
                break;
            case 2:
                units="MB";
                break;
            case 3:
                units:"GB";
                break;
            default:
                units="B";
        }
        return units;  
    }

    static saveFile(user, metadata, files, callback) {

        var max_file_size = 1024 * 1024 * 1024 * 2; //2GB
        //checking file
        if (typeof files !== 'object')
            return callback(new Error("Material is not defined."));
        if (typeof files.file !== 'object')
            return callback(new Error("No material selected."));
        if (typeof files.file.size !== 'number' || files.file.size > max_file_size)
            return callback(new Error("Material is too large."));
        if (typeof files.file.name !== 'string' || files.file.name.match(/\.(com|bat|exe|cmd|msi|vb|vbs|ws|wsf|scf|scr|pif)$/i))
            return callback(new Error("This material was blocked because of security reasons."));
        if (user.free_disk_space < (files.file.size / (1024 * 1024)))
            return callback(new Error("Not enough free disk space."));
    
        var file = files.file;
        var new_path = path.join(__dirname, '../learning_materials');
        //ok
        file.name=sanitize(file.name);
        var file_name = path.parse(file.name).name;
        var file_extension = path.parse(file.name).ext;
        //unique file name
        var unique_id = uniqid((file_name + file_extension).replace('.', '_'));
        //new file name with extension
        var new_filename = unique_id + file_extension;
        //renaming uploaded file
        var new_file_location = path.join(new_path, new_filename);
        fs.rename(file.path, new_file_location);
    
        //creating mongodb document 
        var owner_id = sanitize(user._id);
        var user_free_disk_space = sanitize(user.free_disk_space);
    
        var material = new Material({
            name: file.name,
            extension: file_extension,
            relative_server_path: new_filename,
            owner_id: owner_id,
            size: file.size,
            type: file.type,
            description: metadata.description,
            period_id: metadata.period_id,
            subject_id: metadata.subject_id,
            category_id: metadata.category_id
        });
    
        user_free_disk_space -= file.size / (1024 * 1024);
        material.save(function (err, material) {
            if (err)
                return callback(err);
            if (material) {
                User.findOneAndUpdate({ _id: owner_id }, { $set: { "free_disk_space": user_free_disk_space } }, { new: true }, function (err, user) {
                    if (err || !user) {
                        Material.remove({ "_id": material._id }, function (err) {
                            fs.unlink(new_file_location, function (err) {
                                return callback(new Error("Server Error."));
                            });
                        });
                    } else {
                        return callback(null, user_free_disk_space, metadata);
                    }
                });
            } else {
                fs.unlink(new_file_location, function (err) {
                    return callback(new Error("Server Error."));
                });
            }
        });
    }


    static findRecommendedMaterials(material_id,arr,final_callback){
        async.waterfall([
            function (callback) {

                if(SecurityHelper.areArrayElementsStringAndObjectId(arr)){
                    arr=arr.reverse();

                    Material.find({ "_id": { $in: arr } }, ["period_id"], function (err, searched_materials) {//,"subject_id"
                        if (err)
                            return callback(new Error("Internal error!"));

                        if (searched_materials.length > 0) {
                            var ordered_materials = [];

                            for (var i = 0; i < arr.length; i++) {
                                for (var j = 0; j < searched_materials.length; j++) {
                                    if (searched_materials[j]._id == arr[i]) {
                                        ordered_materials.push(searched_materials[j]);
                                        break;
                                    }
                                }
                            }

                            callback(null, ordered_materials);
                        }
                        else
                            callback(null, null);
                    });
                } 
                else
                    callback(null, null);

            },
            function (ordered_materials, callback) {
                var random_materials_num = 7;
                var num_of_materials = 3;
                var materials = [];
                var materials_ids = [mongoose.Types.ObjectId(sanitize(material_id))];

                if(ordered_materials == null && ordered_materials.length <=0)
                    return callback(null, materials, random_materials_num, materials_ids);

                async.eachSeries(ordered_materials, function (item, loop_callback) {
                    Material.find({ "period_id": item.period_id, "_id": { $nin: materials_ids } }, function (err, similar_materials) { //,"subject_id":item.subject_id
                        if (err)
                            return loop_callback(err);

                        var results_length = similar_materials.length;
                        if (results_length>0) {

                            var x = (num_of_materials > results_length) ? results_length : num_of_materials;

                            for (var i = 0; i < x; i++) {
                                var index;
                                var flag = true;
                                while (flag) {
                                    index = Math.floor(Math.random() * (results_length));
                                    if (materials_ids.indexOf(similar_materials[index]._id) < 0)
                                        flag = false;
                                }

                                materials_ids.push(similar_materials[index]._id);
                                materials.push(similar_materials[index]);
                                random_materials_num--;
                            }
                            num_of_materials--;
                        }
                        loop_callback();
                    });
                },
                function (err) {
                    if (err)
                        return callback(err);

                    callback(null, materials, random_materials_num, materials_ids);
                });
            },
            function (materials, random_materials_num, materials_ids, callback) {

                Material.find({ "_id": { $nin: materials_ids } }, function (err, results) {
                    if (err)
                        return callback(err);
                    //find random indexes
                    if (results.length > 0) {

                        if (random_materials_num < results.length) {

                            while (random_materials_num > 0) {
                                var index = Math.floor(Math.random() * (results.length - 1));

                                if (materials_ids.indexOf(results[index]._id) > -1)
                                    continue;

                                materials_ids.push(results[index]._id);
                                materials.push(results[index]);
                                random_materials_num--;
                            }
                        } else {
                            for (var i = 0; i < results.length; i++) {
                                materials.push(results[i]);
                            }
                        }
                    }
                    callback(null, materials);
                });
            }
        ],
        function (err, result) {
            final_callback(err,result);
        });
    }
}

module.exports=MaterialHelper;