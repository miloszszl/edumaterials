var SecurityHelper=require('./SecurityHelper');

class SubjectHelper{
    static prepareSubjectsAutocomplete(subjects){
        var my_arr=[];
        for(var i=0;i<subjects.length;i++){
            my_arr.push(SecurityHelper.xssClearSubjectAutocomplete(subjects[i]));
        }
        return my_arr;
    }
}

module.exports=SubjectHelper;