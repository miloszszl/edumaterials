var xss = require('xss');
var sanitize = require('mongo-sanitize');
var MyRegex=require('../config/regex');
var mongoose = require('mongoose');

class SecurityHelper{
    static xssClearNamesInArray(my_arr){
        var arr_length=my_arr.length;
        for(var i=0;i<arr_length;i++){
            if(typeof my_arr[i]!=='undefined' && my_arr[i] && typeof my_arr[i].name==='string')
                my_arr[i].name=xss(my_arr[i].name);
        }
    }

    static sanitizeIds(my_arr){
        var arr_len = my_arr.length;
        
        for (var i = 0; i < arr_len; i++) {
            my_arr[i] = mongoose.Types.ObjectId(sanitize(my_arr[i]));
        }
        return my_arr;
    }

    static areArrayElementsStringAndObjectId(my_arr){
        var arr_len=my_arr.length;
        for(var i=0;i<arr_len;i++){
            if(typeof my_arr[i]!=='string' || !MyRegex.ObjectId_regex.test(my_arr[i]))
                return false;
        }
        return true;
    }

    static uniqueElements(my_arr){
        var unique_elements=[];
        var my_arr_len=my_arr.length;
        for(var i=0;i<my_arr_len;i++){
            if(unique_elements.indexOf(my_arr[i])<0){
                unique_elements.push(my_arr[i]);
            }
        }
        return unique_elements; 
    }

    static xssClearUserForAdmin(user){
        var xss_clear_searched_user={
            _id:user._id+"",
            origin:xss(user.origin),
            nickname:xss(user.nickname),
            email:xss(user.email),
            first_name:xss(user.first_name),
            surname:xss(user.surname),
            avatar:xss(user.avatar),
            username:xss(user.username),
            banned:user.banned,
            role_id:user.role_id+"",
            role_name:xss(user.role_name),
            free_disk_space:xss(user.free_disk_space.toFixed(2))
        }
        return xss_clear_searched_user;
    }

    static xssClearUserForThumbnail(user){
        return {nickname:xss(user.nickname),avatar:xss(user.avatar),_id:user._id};
    }

    static xssClearMaterialAutocomplete(material){
        return {label:xss(material.name),value:xss(material.name),id:material._id,category:"materials"};
    }

    static xssClearUserAutocomplete(user){
        return {label:xss(user.nickname),value:xss(user.name),id:user._id,category:"users"};
    }

    static xssClearPeriodAutocomplete(period){
        return {label:xss(period.name),value:xss(period.name),id:period._id};
    }

    static xssClearSubjectAutocomplete(subject){
        return {label:xss(subject.name),value:xss(subject.name),id:subject._id};
    }

    static xssClearCategoryAutocomplete(category){
        return {label:xss(category.name),value:xss(category.name),id:category._id};
    }

    static clearArray(my_array){
        var arr=[];
        if(Array.isArray(my_array)){
          var arr_len=my_array.length;
          for(var i=0;i<arr_len;i++){
            if(typeof my_array[i] !=='undefined' && my_array[i]){
              arr.push({_id:xss(my_array[i]._id),name:xss(my_array[i].name)});
            }
          }
        }
        return arr;
    }
}

module.exports=SecurityHelper