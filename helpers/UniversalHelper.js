
function stringifyIds(my_array){
    var ids=[];
    if(my_array.constructor === Array){
        for(var i=0;i<my_array.length;i++){
            ids.push(JSON.stringify(my_array[i]));
        }
    }
    return ids;
}

function checkIfElementIsInArray(my_array,element){
    if(my_array.indexOf(JSON.stringify(element))>=0){
        return true;
    }
    return false;
}

module.exports={
    checkIfElementIsInArray:checkIfElementIsInArray,
    stringifyIds:stringifyIds,
}