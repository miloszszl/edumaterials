
var sanitize = require('mongo-sanitize');

class MetadataParser{

    static createExactString(str){
        return /^/.source+str+/$/.source;
    }

    static createBeginningString(str){
        return /^/.source+str;
    }

    static createAcronymString(str){
        var letters=str.split("");
        var regex="^";
        
        if(letters.length>1)
            regex+=letters.join(/\S*\s/.source);
        else if(letters.length==1)
            regex+=letters[0];
        
        regex+=/.*$/.source;

        return regex;
    }

    static createRegexObject(str){
        str=sanitize(str.trim());
        var metadata_object={};
        metadata_object.exact_name=this.createExactString(str);
        metadata_object.name_beginning=this.createBeginningString(str);
        metadata_object.name_acronym=this.createAcronymString(str);

        return metadata_object;
    }

    // static splitData(text){
    //     var text_arr=text.split(",");

    //     var text_arr2=[];
    //     for(var i=0;i<text_arr.length;i++){
    //         text_arr2.concat(text_arr[i].split(/\r?\n/));
    //     }
    //     return text_arr2;
    // }
}

module.exports=MetadataParser;