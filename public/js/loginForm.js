$(document).ready(function(){
    
    $("#loginForm").submit(function(e) {
        e.preventDefault();
        
        var el=document.getElementById("loginAlert");
        el.style.display="none";

        var url = "/users/login"; // the script where you handle the form input.
        
        $.ajax({
            type: "POST",
            url: url,
            data: $("#loginForm").serialize(), // serializes the form's elements.
            success: function(data)
            {
                if(data.code==0 || data.code==2){
                    var el=document.getElementById("loginAlert");
                    el.style.display="block";
                    el.innerHTML=data.resp;
                }else if(data.code==1){
                    var href=window.location.href;
                    window.location.href=href;
                }
            }
        });
    });
});