function replaceMenuInputs(type){
    replacePeriodInput(type);
    replaceSubjectInput(type);
    replaceCategoryInput(type);
}

function switchActiveClass(el1,el2){
    $(el1).removeClass("active");
    $(el2).addClass("active");
}

function replacePeriodInput(type){
    if(type=="text")
        $("#select_period").replaceWith('<textarea id="periods_text" rows="4" cols="20" name="periods_text" placeholder="Comma separated periods" class="form-control"></textarea>');
    else if(type=="select"){
        var url="/materials/periods";
        
        $.ajax({
            type: "GET",
            url: url,
            success:function(data){
                if(data.code==1){

                    var periods=data.periods;
                    var periods_len=periods.length;
                    var periods_part="";
                    for(var i=0;i<periods_len;i++){
                        periods_part+="<option value="+periods[i]._id+" >"+periods[i].name+"</option>";
                    }

                    $("#periods_text").replaceWith(`<select multiple name="periods" id="select_period" class="form-control hand select_box">`
                                                    +periods_part+`</select>`);
                }else{
                    showSnackbar("error",data.resp);
                }
            }
        });
    }
}

function applyFilter(){
    var periods=$("#periods_text").val();
    var subjects=$("#subjects_text").val();
    var categories=$("#categories_text").val();
}

function replaceSubjectInput(type){
    if(type=="text")
        $("#select_subject").replaceWith('<textarea id="subjects_text" rows="4" cols="20" name="subjects_text" placeholder="Comma separated subjects" class="form-control"></textarea>');
    else if(type=="select")
        $("#subjects_text").replaceWith(`<select multiple name="subjects" id="select_subject" class="form-control hand select_box" disabled></select>`);
}

function replaceCategoryInput(type){
    if(type=="text")
        $("#select_category").replaceWith('<textarea id="categories_text" rows="4" cols="20" name="categories_text" placeholder="Comma separated categories" class="form-control"></textarea>');
    else if(type=="select")
        $("#categories_text").replaceWith(`<select multiple name="categories" id="select_category" class="form-control hand select_box" disabled></select>`);
}