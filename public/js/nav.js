$(document).ready(function(){

    var url;
    var parts=location.pathname.split("/");

    url="/";
    if(parts.length==2 || (parts.length>=2 && parts[1]=='admin_panel')){
        url=parts[1];
    }else if(parts.length>2 && ((parts[1]=='users') || (parts[1]=='materials')))
        url=parts[1]+'/'+parts[2];
    
    $('a.nav-link.main_nav[href="/' + url + '"]').addClass('active');

    if(parts.length>2 && parts[1]=='users'){
        var user_url="";
        for(var i=1;i<parts.length;i++){
            user_url+='/'+parts[i];
        }
        $('a.nav-link.user_profile_nav[href="' + user_url + '"]').addClass('active');
    }
    
    if(parts.length>2 && parts[1]=='admin_panel'){
        var admin_url=parts[1]+'/'+parts[2];        
        $('a.col.adminNav[href^="/' + admin_url + '"]').addClass('admin_nav_active');
    }
    
});