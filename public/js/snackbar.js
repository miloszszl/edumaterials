var sync_flag=true;

var snackbar={
    show:function(type,text){

        snackbar_element=document.getElementById("snackbar");
        snackbar_element.innerHTML=text;

        if(sync_flag==true){
            sync_flag=false;

            if(type=="error"){
                snackbar_element.classList.remove("bg_grey");
                snackbar_element.classList.add("bg_failure");
            }else if(type=="ok"){
                snackbar_element.classList.remove("bg_failure");
                snackbar_element.classList.add("bg_grey");
            }
    
            snackbar_element.classList.add("show");
            setTimeout(function(){snackbar_element.classList.remove("show");sync_flag=true;}, 3000);
        }
    }
}
