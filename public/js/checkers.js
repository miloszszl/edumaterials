function checkTextLengthMax(my_text,length){
    if(my_text.val().length>length)
        return false;
    else
        return true;
}

function checkTextLengthMinMax(my_text,min,max){
    if(my_text.val().length>max || my_text.val().length<min)
        return false;
    else
        return true;
    
}