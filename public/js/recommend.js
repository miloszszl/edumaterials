function trackUserChoices(){
    var choices_cookie=getCookieByName('user_choices');
    var user_choice=getUserChoice();
    if(choices_cookie==undefined){
        var arr=[user_choice];
        var json_str = JSON.stringify(arr);
        createCookie('user_choices', json_str, 10);
    }else{
        var arr = JSON.parse(choices_cookie);
        
        var index=arr.indexOf(user_choice);
        if(index>=0){
            arr.splice(index,1);
        }
        
        while(true){
            if(arr.length>=3){
                arr.splice(0,1);
            }else{
                break;
            }
        }
        arr.push(user_choice);
        var json_str = JSON.stringify(arr);
        createCookie('user_choices', json_str, 10);   
    }  
}

function getUserChoice(){
    var material_id=document.getElementById("material_name_row").getAttribute("data-material_id");
    return material_id;  
}