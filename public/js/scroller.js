

function scrollSmoothly(href,top=false){
    if (href !== "") {
        if(top==true){
            $('html, body').animate({
                scrollTop: 0
            }, 800);
        }
        else{
            $('html, body').animate({
                scrollTop: $(href).offset().top-80
            }, 800);
        }  
    } 
}
