    function rmDisabled(id){
        var isDisabled = $(id).prop('disabled');
        if(isDisabled){
            $(id).prop('disabled',false);
        }
    }


    function findElement(class_name,new_value){
        var elements=$(class_name);
        var len=elements.length;
        if(len>0){
            var element=$(elements[0]);
            var child_input;
            for(var i=0;i<len;i++){
                
                child_input=$(elements[i]).find("input.name_input");
                if(typeof $(child_input)[0].value==='string' && (new_value.toLowerCase()>$(child_input)[0].value.toLowerCase()))
                    continue;
                
                if(i==0)
                    return null;
                else
                    return elements[i-1];
            }
            return elements[len-1];
        }else
            return null;
    }