
$(document).ready(function(){

    var cookieName='cookiesAgreement';
    var cookieVal='yes';
    
    if(getCookieByName(cookieName)!==cookieVal){     //if there is no cookie with name 'cookiesAgreement' then getCookieByName returns undefined
        $('#cookieInfo').animate({
                opacity: 1, 
                bottom: '-5px',
            }, 'slow', 'linear');
    } 

    $("#cookieButton").click(function(event){
        $('#cookieInfo').animate({
            opacity: 0, 
            marginBottom: '-100px'
        }, 'slow', 'linear', function() {
            createCookie(cookieName,cookieVal,30);
            $(this).remove();
        });
    });
    
});