    
    var endpoint_names={
        "fav":1,
        "my_f":2
    }
    
    $(document).on('click','.download_btn',function(e){
        e.preventDefault();

        var file_id=$(this).data('file_id');
        window.location='/materials/'+file_id+'/download';

        var download_counter=$("#download_"+file_id);
        var download_counter_length=download_counter.length;
        if(download_counter_length>0){
            var val=parseInt(download_counter.text());
            download_counter.text(val+1);
        }
    });

    $(document).on('click','.like_btn',function(e){
        e.preventDefault();

        var button=$(this);
        var operation_type=button.data("operation_type");
        var file_id=button.data('file_id');
        var url;

        if(operation_type=="like"){
            url='/materials/'+file_id+'/like';
        }else{
            url='/materials/'+file_id+'/remove_like';
        }

        $.post(url,function(data){
            
            if(data.code==1){
                var like_counter=$("#like_"+file_id);
                var like_counter_length=like_counter.length;
                
                var val=0;
                
                if(operation_type=="like"){
                    button.find('.tooltiptext').text('Remove like');
                    
                    button.addClass('like_color');
                    button.data("operation_type","remove_like");
                    val=1;
                }else{
                    button.find('.tooltiptext').text('Like');
                    
                    button.removeClass('like_color');
                    button.data("operation_type","like");
                    val=-1;
                }

                if(like_counter_length>0){
                    var current_val=parseInt(like_counter.text());
                    like_counter.text(current_val+val);
                }
                snackbar.show("ok",data.resp);
                
            }else{
                snackbar.show("error",data.resp);
            }
        });
    });

    $(document).on('click','.favourite_btn',function(e){
        e.preventDefault();
        
        var button=$(this);
        var operation_type=button.data("operation_type");
        var file_id=button.data('file_id');
        var url,title,color;
        
        if(operation_type=="add"){
            url='/favourite/'+file_id+'/add';
        }else{
            url='/favourite/'+file_id+'/remove';
        }
            
        $.post(url,function(data){
                
                if(data.code==1){
                    if(operation_type=="add"){
                        button.find('.tooltiptext').text('Remove from fafourite');
                        button.addClass('gold_color');
                        button.data("operation_type","remove");
                    }else{
                        button.find('.tooltiptext').text('Add to favourite');
                        button.removeClass('gold_color');
                        button.data("operation_type","add");
                    }

                    snackbar.show("ok",data.resp);
                }else{
                    snackbar.show("error",data.resp);
                }


        });
    });

    function findClassName(f_type,f_extension){
        if(typeof f_type==='string' && typeof f_extension==='string'){

            
            if(f_type.indexOf('audio/') !== -1){
                return "fa-file-audio-o";

            }else if(f_type.indexOf('image/') !== -1){
                return "fa-file-photo-o";

            }else if(f_type.indexOf('video/') !== -1){
                return "fa-file-video-o";

            }else if(f_type.indexOf('text/plain') !== -1){
                return "fa-file-text-o";

            }else if(f_type.indexOf('application/pdf') !== -1){
                return "fa-file-pdf-o";   

            }else if(f_type.indexOf('application/msword') !== -1 ||
            f_type.indexOf('application/vnd.openxmlformats-officedocument.wordprocessingml.document') !== -1||
            f_type.indexOf('application/vnd.openxmlformats-officedocument.wordprocessingml.template') !== -1||
            f_type.indexOf('application/vnd.ms-word.document.macroEnabled.12') !== -1){
                return "fa-file-word-o";   

            }else if(f_type.indexOf('application/vnd.ms-excel') !== -1 ||
            f_type.indexOf('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') !== -1 || 
            f_type.indexOf('application/vnd.openxmlformats-officedocument.spreadsheetml.template') !== -1 ||
            f_type.indexOf('application/vnd.ms-excel.sheet.macroEnabled.12') !== -1 ||
            f_type.indexOf('application/vnd.ms-excel.template.macroEnabled.12') !== -1 ||
            f_type.indexOf('application/vnd.ms-excel.addin.macroEnabled.12') !== -1 ||
            f_type.indexOf('application/vnd.ms-excel.sheet.binary.macroEnabled.12') !== -1 ){
                return "fa-file-excel-o";   

            }else if(f_type.indexOf('application/vnd.ms-powerpoint') !== -1 || 
            f_type.indexOf('application/vnd.openxmlformats-officedocument.presentationml.presentation') !== -1 ||
            f_type.indexOf('application/vnd.openxmlformats-officedocument.presentationml.template') !== -1 ||
            f_type.indexOf('application/vnd.openxmlformats-officedocument.presentationml.slideshow') !== -1 ||
            f_type.indexOf('application/vnd.ms-powerpoint.addin.macroEnabled.12') !== -1 ||
            f_type.indexOf('application/vnd.ms-powerpoint.presentation.macroEnabled.12') !== -1 ||
            f_type.indexOf('application/vnd.ms-powerpoint.template.macroEnabled.12') !== -1 ||
            f_type.indexOf('application/vnd.ms-powerpoint.slideshow.macroEnabled.12') !== -1){
                return "fa-file-powerpoint-o";

            }else if(f_extension.indexOf('zip') !== -1 || f_extension.indexOf('rar')!== -1 ){
                return "fa-file-zip-o";

            }else if(f_type.indexOf('text/') !== -1 || f_type.indexOf('application/')!== -1 ){
                return "fa-file-code-o";

            }
        }
        return "fa-file-o";
    }


    function generateThumbnailsHtml(materials){
        var html='';
        var f_name,f_type,element,class_name,f_extension,f_id,f_is_favourite,f_is_liked;//,f_likes,f_downloads,f_units,f_size

        if(typeof materials==='undefined')
            return "";

        for(var i=0;i<materials.length;i++){
            
            element=materials[i];

            f_id=element.id;
            f_name=element.name;
            f_extension=element.extension;
            f_type=element.type;
            f_is_favourite=element.is_favourite;
            f_is_liked=element.is_liked;

            class_name=findClassName(f_type,f_extension)

            var favourite_class="";
            var favourite_title="Add to favourite";
            var operation_type="add";

            var like_class="";
            var like_title="Like";
            var operation_type_like="like";
            
            if(f_is_favourite==true){
                favourite_class="gold_color";
                favourite_title="Remove from favourite";
                operation_type="remove";
            }

            if(f_is_liked==true){
                like_class="like_color";
                like_title="Remove like";
                operation_type_like="remove_like";
            }

            html+=
            `<div class="thumbnail_box"><a href="/materials/`+f_id+`"> <i aria-hidden="true" title="Show details" class="fa `+class_name+` thumbnail_icon" ></i></a>
                <div class="thumbnail_buttons">
                    <button data-file_id="`+f_id+`" data-operation_type="`+operation_type_like+`" class="btn btn-primary t_button `+like_class+` like_btn my_tooltip"><i aria-hidden="true" class="fa fa-thumbs-up"></i><span class="tooltiptext">`+like_title+`</span></button>
                    <button data-file_id="`+f_id+`" class="btn btn-primary t_button download_btn my_tooltip"><i aria-hidden="true" class="fa fa-cloud-download"></i><span class="tooltiptext">Download</span></button>
                    <button data-file_id="`+f_id+`" data-operation_type="`+operation_type+`" class="btn btn-primary t_button `+favourite_class+` favourite_btn my_tooltip"><i aria-hidden="true" class="fa fa-star" ></i><span class="tooltiptext">`+favourite_title+`</span></button>
                </div>
                <div class="file_name">
                    <div>`
                    +f_name+
                    `</div>
                    <div class="additional_thumbnail_data">
                        <div>Period: `
                            +element.period_name+
                        `</div>
                        <div>Subject: `
                            +element.subject_name+
                        `</div>
                        <div>Category: `
                            +element.category_name+
                        `</div>
                    </div>
                </div>
            </div>`
        }
        return html;
    };

    function load_elements(my_mutex,page,my_window,finished,endpoint){
        if ($(document).height() - my_window.height() <= my_window.scrollTop()+20) {
            if(my_mutex.val==true && finished.val==false){
                my_mutex.val=false;

                $('#loading').show();
                
                var user_id=$("#searched_user_id").attr('value');
                
                if(endpoint==endpoint_names.fav){
                    var url="/users/"+user_id+"/favourite/"+page.num;
                }else if(endpoint==endpoint_names.my_f){
                    var url="/users/"+user_id+"/uploaded_materials/"+page.num;
                }

                $.ajax({
                    url:url,
                    method:"GET",
                    async: true
                }).done(function(data){
                    if(data.resp==""){
                        finished.val=true;
                    }else{
                        var html=generateThumbnailsHtml(data.resp);
                        page.num++;
                        $(".all_thumbnails").append(html);
                    }
                    my_mutex.val=true;
                    $('#loading').hide();
                });  
            }
        }
    };

    function load_elements_by_criteria(my_mutex,page,my_window,finished,periods,subjects,categories,sort_option,type){

        if(my_mutex.val==true && finished.val==false){
            my_mutex.val=false;
        
            $('#loading').show();
            
            var sort_option=$('#sort_form input[name="sort_option"]:checked').val();

            var url,data;
            if(type=="select"){
                url="/materials/find_by_criteria/"+page.num;
                data={
                    period_ids:periods,
                    subject_ids:subjects,
                    category_ids:categories,
                    sort_option:sort_option
                }
            }else{
                url="/materials/find_by_text_criteria/"+page.num;
                data={
                    periods_text:periods,
                    subjects_text:subjects,
                    categories_text:categories,
                    sort_option:sort_option
                }
            }

            $.ajax({
                type:"POST",
                data:data,
                url:url,
                async: true}).
                done(function(data){
                    if(data.code==1){
                        var html=generateThumbnailsHtml(data.resp);
                        page.num++;
                        $(".all_thumbnails").append(html);
                    }
                    else if(data.code==2){
                        finished.val=true;
                    }else if(data.code==3){
                        $(".all_thumbnails").empty();
                        var html=generateThumbnailsHtml(data.resp);
                        page.num++;
                        $(".all_thumbnails").append(html);
                    }else{
                        snackbar.show("error",data.resp);
                    }
                    my_mutex.val=true;
                    $('#loading').hide();
            });
        }
    }