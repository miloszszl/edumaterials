var express = require('express');
var router = express.Router();
var Material = require('../models/material')
var ConfigBruteForce = require('../config/bruteforce');
var UniversalHelper = require('../helpers/UniversalHelper');
const nodemailer = require('nodemailer');
var sanitize = require('mongo-sanitize');
var User = require('../models/user');
var xss = require('xss');
var Period = require('../models/period');
var SecurityHelper = require('../helpers/SecurityHelper');
var MaterialHelper = require('../helpers/MaterialHelper');
var UserHelper = require('../helpers/UserHelper');
var validator = require('validator');
var EmailSender = require('../helpers/EmailSender');

router.get('/start', function (req, res, next) {
	Material.count({}, function (err, c) {
		if (err)
			return next(err);

		Period.find({}, {}, { sort: { "name": 1 } }, function (err, periods) {
			if (err)
				return next(err)

			SecurityHelper.xssClearNamesInArray(periods);
			res.render('home', { periods: periods, materials_length: c });
		});
	});
});

router.get('/', function (req, res, next) {
	res.render('start_page');
});

router.get('/search', function (req, res, nest) {

	if (typeof req.query.term !== 'string' || req.query.term.length <= 0)
		return res.redirect("/start");

	var query = sanitize(req.query.term);
	Material.find({ name: { "$regex": query, "$options": "i" } }, {}, { sort: { "name": 1 } }, function (err, materials) {
		if (err)
			return next(err);

		User.find({ nickname: { "$regex": query, "$options": "i" } }, {}, { sort: { "nickname": 1 } }, function (err, users) {
			if (err)
				return next(err);

			var favourite_materials_ids = [], liked_materials_ids = [];

			if (req.isAuthenticated() && typeof req.user !== 'undefined') {
				favourite_materials_ids = UserHelper.findFavouriteMaterialsIds(req.user);
				liked_materials_ids = UserHelper.findLikedMaterialsIds(req.user);
			}

			MaterialHelper.prepareMaterials(materials, favourite_materials_ids, liked_materials_ids, function (err, result) {
				if (err)
					return next(err);

				var xss_clear_users = UserHelper.prepareUsersForThumbanils(users);
				res.render('search_results', { query: xss(query), materials: JSON.stringify(result), users: xss_clear_users });
			});
		});
	});
});

router.get('/query', function (req, res, next) {

	if (typeof req.query.term !== 'string')
		return res.send(JSON.stringify([]));

	var query = sanitize(req.query.term);

	Material.find({ name: { "$regex": query, "$options": "i" } }, "name", { limit: 4, sort: { "name": 1 } }, function (err, materials) {
		if (err)
			return res.send(JSON.stringify([{ label: "ERROR", value: "ERROR" }]));

		User.find({ nickname: { "$regex": query, "$options": "i" } }, "nickname", { limit: 4, sort: { "nickname": 1 } }, function (err, users) {
			if (err)
				return res.send(JSON.stringify([{ label: "ERROR", value: "ERROR" }]));

			var materials_arr = MaterialHelper.prepareMaterialsAutocomplete(materials);
			var users_arr = UserHelper.prepareUsersAutocomplete(users);
			var my_arr = materials_arr.concat(users_arr);

			res.send(JSON.stringify(my_arr));
		});
	});
});

router.post('/contact', ConfigBruteForce.contactBruteForce.prevent, function (req, res, next) {

	if (typeof req.body.fullName !== 'string' || req.body.fullName.length < 1 || req.body.fullName.length > 200)
		return res.send({ code: 0, resp: "Full name should be 1-200 characters long." });

	if (typeof req.body.topic !== 'string' || req.body.topic.length < 1 || req.body.topic.length > 200)
		return res.send({ code: 0, resp: "Topic is required. Should be 1-200 characters long." });

	if (typeof req.body.email !== 'string' || !validator.isEmail(req.body.email))
		return res.send({ code: 0, resp: "Email is Required. Must be valid email." });

	if (typeof req.body.content !== 'string' || req.body.content.length < 1 || req.body.content.length > 600)
		return res.send({ code: 0, resp: "Content is Required. Should be 1-600 characters long." });

	var transporter = EmailSender.createTransporter();

	var mailOptions = {
		from: req.body.fullName,
		to: 'miloszszlachetka@gmail.com',
		subject: req.body.topic,
		text: req.body.content + '\n\n' + req.body.fullName+'\n' + req.body.email
	};

	if (req.app.get('env') === 'development')
		process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

	transporter.sendMail(mailOptions, function (err) {
		if (err)
			return res.send({ code: 0, resp: "Internal error!" });

		return res.send({ code: 1, resp: "Message has been sent.<br>Thank you!" })
	});
});

module.exports = router;