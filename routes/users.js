var express = require('express');
var router = express.Router();
var request = require('request');
var User = require('../models/user');
var mongoose = require('mongoose');
var ConfigBruteForce = require('../config/bruteforce');
var passport = require('../config/passport');
var MyAuth = require('../config/my_auth');
var validator = require('validator');
var UniversalHelper = require('../helpers/UniversalHelper');
var sanitize = require('mongo-sanitize');
var async = require('async');
var crypto = require('crypto');
var xss = require('xss');
const nodemailer = require('nodemailer');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var uniqid = require('uniqid');
var MyRegex = require('../config/regex');
var Material = require('../models/material');
var configEnv = require('../config/environment');
var csrf = require('csurf');
var MaterialHelper = require('../helpers/MaterialHelper');

var csrfProtection = csrf({ cookie: true })

function logInLocalAjax(req, res, next) {
	passport.authenticate('local', function (err, user, info) {
		if (err) { return next(err); }
		if (info !== undefined) {
			var data = {
				code: 2,
				resp: info.message
			}
			return res.send(data);
		}
		if (!user) {
			var data = {
				code: 0,
				resp: "Invalid username or password."
			}
			return res.send(data);
		}

		if (user && user.banned == true) {
			var data = {
				code: 0,
				resp: "Your account is banned."
			}
			return res.send(data);
		}

		req.logIn(user, function (err) {
			if (err) { return next(err); }
			req.brute.reset();
			req.flash('success_msg', 'You are logged in.');
			var data = {
				code: 1,
			}
			res.send(data);
		});

	})(req, res, next);
}

function loginLocalPug(req, res, next) {
	passport.authenticate('local', function (err, user, info) {
		if (err) { return next(err); }

		if (!user || info !== undefined) {
			req.flash('error_msg', 'Invalid username or password.');
			return res.redirect('/start')
		}

		req.logIn(user, function (err) {
			if (err) { return next(err); }
			req.flash('success_msg', 'You are logged in.');
			res.redirect('/start')
		});
	})(req, res, next);
}


router.post('/login', ConfigBruteForce.globalBruteforce.prevent, ConfigBruteForce.userBruteforce.getMiddleware({
	key: function (req, res, next) {
		// prevent too many attempts for the same username 
		next(req.body.username);
	}
}), logInLocalAjax
);

// Redirect the user to Facebook for authentication.  When complete,
// Facebook will redirect the user back to the application at
//     /auth/facebook/callback
router.get('/auth/facebook', passport.authenticate('facebook'));
//app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

// Facebook will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
router.get('/auth/facebook/callback',
	passport.authenticate('facebook', {
		failureRedirect: '/start',
		failureFlash: true
	}), function (req, res) {
		req.flash('success_msg', 'You are logged in.');
		res.redirect('/start');
	});

// GET /auth/google
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Google authentication will involve redirecting
//   the user to google.com.  After authorization, Google will redirect the user
//   back to this application at /auth/google/callback
router.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
router.get('/auth/google/callback',
	passport.authenticate('google', {
		failureRedirect: '/start',
		failureFlash: true
	}), function (req, res) {
		req.flash('success_msg', 'You are logged in.');
		res.redirect('/start');
	});


// Register
router.get('/register', MyAuth.ensureNotAuthenticated, csrfProtection, function (req, res) {
	res.render('register', { csrfToken: req.csrfToken() });
});

router.post('/register', MyAuth.ensureNotAuthenticated, csrfProtection, function (req, res, next) {
	var special_chars = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
	var lowercase_letters = /[a-z]+/;
	var uppercase_letters = /[A-Z]+/;
	var numbers = /[0-9]+/;

	var err_msg = '';

	if (typeof req.body.username !== 'string' || !validator.isLength(req.body.username, { min: 4, max: 30 })) {
		err_msg += "Username length should be 4-30 characters long. \n";
	}
	if (typeof req.body.password !== 'string' || !validator.isLength(req.body.password, { min: 6, max: 30 }) || !special_chars.test(req.body.password) || !lowercase_letters.test(req.body.password) || !uppercase_letters.test(req.body.password) || !numbers.test(req.body.password)) {
		err_msg += "Password should be 6-30 characters long.Password must include one lowercase character, \
		one uppercase character, a number, and a special character. \n";
	}
	if (typeof req.body.password2 !== 'string' || !validator.equals(req.body.password, req.body.password2)) {
		err_msg += "Passwords do not match. \n";
	}
	if (typeof req.body.email !== 'string' || !validator.isEmail(req.body.email)) {
		err_msg += "Email must be valid. \n";
	}
	if (typeof req.body.nickname !== 'string' || !validator.isLength(req.body.nickname, { min: 1, max: 30 })) {
		err_msg += "Nickname length should be 1-30 characters long. \n";
	}
	if (typeof req.body.first_name !== undefined && req.body.first_name) {
		if (typeof req.body.first_name !== 'string' || !validator.isLength(req.body.first_name, { min: 0, max: 50 })) {
			err_msg += "First name length should be 0-50 characters long. \n";
		}
	}
	if (typeof req.body.surname !== undefined && req.body.surname) {
		if (typeof req.body.surname !== 'string' || !validator.isLength(req.body.surname, { min: 0, max: 50 })) {
			err_msg += "Surname length should be 0-50 characters long. \n";
		}
	}

	var username = sanitize(req.body.username);

	User.getUserByUsername(username, function (err, user) {
		if (err)
			return next(err);

		if (user)
			err_msg += "This username is occupied. \n";

		var email = sanitize(req.body.email).toLowerCase();

		User.findOne({ email: email }, function (err, result) {
			if (err)
				return next(err);

			if (result)
				err_msg += "Email must be unique. \n";

			if (req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null)
				err_msg += "reCaptcha is not checked. \n";


			//post request to google api
			request.post(
				'https://www.google.com/recaptcha/api/siteverify',
				{
					form:
						{
							secret: '6LctoS0UAAAAAId7BrZBiv7KIdcEDCX7wRdu7tDo',
							remoteip: req.connection.remoteAddress,
							response: req.body['g-recaptcha-response']
						}
				},
				function (err, response, body) {

					if (err)
						return next(err);

					if (typeof body.success === undefined && body.success == false) {
						err_msg += "Failed recaptcha verification. \n";
					}

					if (err_msg != '') {
						res.render('register', { registration_err_msg: err_msg });
					} else {

						//username is sanitized already
						var password = sanitize(req.body.password);
						var nickname = sanitize(req.body.nickname);
						var first_name = sanitize(req.body.first_name);
						var surname = sanitize(req.body.surname);

						var newUser = new User({
							username: username,
							password: password,
							email: email,
							nickname: nickname,
							avatar: 'default_avatar.png'
						});

						//optional fields
						if (typeof first_name !== undefined && first_name && first_name != '');
						newUser.first_name = sanitize(first_name);
						if (typeof surname !== undefined && surname && surname != '');
						newUser.surname = sanitize(surname);

						MyAuth.setRoleUser(newUser, function (usr) {
							usr.save(function (err) {	//createUser=save
								if (err)
									throw err;
								else
									loginLocalPug(req, res, next);
							});
						});
					}
				}
			);
		});
	});
});

//Log out
router.get('/logout', MyAuth.ensureAuthenticated, function (req, res) {
	req.logout();
	req.flash('success_msg', 'You are logged out.');
	res.redirect('/start');
});

router.get('/:user_id', MyAuth.ensureAuthenticated, MyAuth.findUserRole, MyAuth.checkIfIsOwner, function (req, res, next) {

	if (typeof req.params.user_id === 'string' && MyRegex.ObjectId_regex.test(req.params.user_id)) {

		var clear_user_id = sanitize(req.params.user_id);

		User.findById(clear_user_id, function (err, user) {
			if (err)
				return next(err);
			if (user) {
				var has_password = false;
				if (typeof user.password !== 'undefined') {
					has_password = true;
				}

				var clear_user = {
					_id: xss(user._id),
					nickname: xss(user.nickname),
					first_name: xss(user.first_name),
					surname: xss(user.surname),
					avatar: xss(user.avatar),
					is_owner: req.user.is_owner,
					has_password: has_password
				}

				if (req.user.role_name == configEnv.admin_role)
					req.user.is_admin = true;

				if (req.user.is_admin || clear_user.is_owner) {
					clear_user.email = xss(user.email);
					clear_user.free_disk_space = xss(user.free_disk_space.toFixed(2));
				}

				var error_msg ="";
				if(user.banned)
					error_msg ="This user is banned!";

				res.render('user_profile', { searched_user: clear_user ,error_msg:error_msg});
			} else {
				res.render('user_profile', { error_msg: "That user doesn't exist." });
			}
		});
	} else {
		res.render('user_profile', { error_msg: "That user doesn't exist." });
	}
});

router.post('/:user_id/reset_pass', MyAuth.ensureAuthenticated, MyAuth.checkIfIsOwner, function (req, res, next) {
	async.waterfall([
		function (callback) {
			if (!req.user.is_owner) {
				var error = new Exrror({ err_code: 401, err_msg: "Unauthorized" });
				return next(error);
			}
			if (typeof req.user.password === 'undefined')
				return res.send({ code: 0, resp: "You have no password.\n " });
			if (typeof req.user.email === 'undefined')
				return res.send({ code: 0, resp: "Email is necessary. \n " });
			if (typeof req.user === 'undefined')
				return res.send({ code: 0, resp: "You must be logged in. \n " });

			crypto.randomBytes(20, function (err, buf) {
				var token = buf.toString('hex');
				callback(err, token);
			});
		},
		function (token, callback) {

			User.findOne({ _id: sanitize(req.user._id), $or: [{ resetPasswordExpires: { $lt: Date.now() } }, { resetPasswordExpires: null }] }, function (err, searched_user) {
				if (err)
					return callback(err);

				if (!searched_user) {
					return res.send({ code: 0, resp: "Email with instructions has been sent earlier. \n " });
				}

				searched_user.resetPasswordToken = token;
				searched_user.resetPasswordExpires = Date.now() + 360000; // 12 h  =3600000*12

				searched_user.save(function (err) {
					callback(err, token, searched_user);
				});
			});
		},
		function (token, user, callback) {
			var transporter = nodemailer.createTransport({
				service: 'gmail',
				port: 465,
				secure: true, // true for 465, false for other ports
				auth: {
					user: 'miloszszl.ematerials@gmail.com',
					pass: 'Q7ber@ppl'
				}
			});

			var mailOptions = {
				to: req.user.email,
				from: 'miloszszl.ematerials@gmail.com',
				subject: 'E-learning materials Password Reset',
				text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
					'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
					'https://' + req.headers.host + '/users/reset_pass/' + token + '\n\n' +
					'If you did not request this, please ignore this email and your password will remain unchanged.\n'
			};

			req.user.email = xss(req.user.email);

			if (req.app.get('env') === 'development')
				process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

			transporter.sendMail(mailOptions, (err, info) => {
				var data = {
					code: 1,
					resp: 'An e-mail with further instructions has been sent to ' + req.user.email + '.'
				}

				callback(err, data);
			});
		}
	], function (err, result) {
		if (err)
			return next(err);

		res.send(result);
	});
});

router.get('/reset_pass/:token', function (req, res) {
	req.params.token = sanitize(req.params.token);

	User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
		if(err)
			return next(err);

		if (!user) 
			return res.render('reset_pass', { error_msg: 'Password reset token is invalid or has expired.' });
		
		res.render('reset_pass', { ok: true });
	});
});

router.post('/reset_pass/:token', function (req, res, next) {
	async.waterfall([
		function (done) {
			req.params.token = sanitize(req.params.token);
			User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
				if (!user) {
					return res.render('reset_pass', { ok: true, error_msg: 'Password reset token is invalid or has expired.' });
				}

				req.body.old_pass = sanitize(req.body.old_pass);

				user.comparePassword(req.body.old_pass, function (err, isMatch) {
					if (err)
						throw err;
					if (!isMatch) {
						return res.render('reset_pass', { ok: true, error_msg: "Wrong old password. " });
					} else {
						var special_chars = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
						var lowercase_letters = /[a-z]+/;
						var uppercase_letters = /[A-Z]+/;
						var numbers = /[0-9]+/;
						var err_msg = '';

						if (typeof req.body.new_pass !== 'string' || !validator.isLength(req.body.new_pass, { min: 6, max: 30 }) || !special_chars.test(req.body.new_pass) || !lowercase_letters.test(req.body.new_pass) || !uppercase_letters.test(req.body.new_pass) || !numbers.test(req.body.new_pass)) {
							err_msg += "Password should be 6-30 characters long.Password must include one lowercase character, \
							one uppercase character, a number, and a special character.\n ";
						}
						if (typeof req.body.confirm_pass !== 'string' || !validator.equals(req.body.new_pass, req.body.confirm_pass)) {
							err_msg += "Passwords do not match.\n";
						}

						if (err_msg != '') {
							return res.render('reset_pass', { ok: true, error_msg: err_msg });
						} else {

							user.password = sanitize(req.body.new_pass);
							user.resetPasswordToken = undefined;
							user.resetPasswordExpires = undefined;

							user.save(function (err) {
								done(err, user);
							});
						}
					}
				});
			});
		},
		function (user, done) {
			var transporter = nodemailer.createTransport({
				service: 'gmail',
				port: 465,
				secure: true, // true for 465, false for other ports
				auth: {
					user: 'miloszszl.ematerials@gmail.com',
					pass: 'Q7ber@ppl'
				}
			});
			var mailOptions = {
				to: user.email,
				from: 'miloszszl.ematerials@gmail.com',
				subject: 'Your password has been changed',
				text: 'Hello,\n\n' +
					'This is a confirmation that the password for your account ' + user.username + ' has just been changed.\n'
			};

			if (req.app.get('env') === 'development')
				process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

			transporter.sendMail(mailOptions, function (err) {
				req.flash('success_msg', 'Success! Your password has been changed.');
				done(err);
			});
		}
	], function (err) {
		if (err)
			return next(err);

		res.redirect('/start');
	});
});


router.post('/:user_id/update', MyAuth.ensureAuthenticated, MyAuth.checkIfIsOwner, function (req, res, next) {

	if (!req.user.is_owner) {
		return res.send({ code: 0, resp: "Unauthorized." });
	}

	var max_file_size = 2 * 1024 * 1024;  //2MB
	var form = new formidable.IncomingForm();
	form.multiples = false;

	form.parse(req, function (err, fields, files) {

		if (typeof fields.nickname !== 'string' || !validator.isLength(fields.nickname, { min: 1, max: 30 })) {
			return res.send({ code: 0, resp: "Nickname length should be 1-30 characters long." });
		}

		if (typeof fields.first_name !== 'string' || !validator.isLength(fields.first_name, { min: 0, max: 50 })) {
			return res.send({ code: 0, resp: "First name length should be 0-50 characters long." });
		}

		if (typeof fields.surname !== 'string' || !validator.isLength(fields.surname, { min: 0, max: 50 })) {
			return res.send({ code: 0, resp: "Surname length should be 0-50 characters long." });
		}

		var id = sanitize(req.user.id);
		var sanitized_surname = sanitize(fields.surname);
		var sanitized_nickname = sanitize(fields.nickname);
		var sanitized_first_name = sanitize(fields.first_name);

		if (fields.is_avatar_removed == "false" && typeof files !== 'undefined' && files && typeof files.avatar !== 'undefined' && files.avatar && files.avatar.size > 0) {
			if (files.avatar.size > max_file_size) {
				return res.send({ code: 0, resp: "File is to big." });
			} else {
				if (files.avatar.name.match(/\.(jpg|jpeg|png)$/i)) {

					//getting file name and extension
					var file_name = path.parse(files.avatar.name).name;
					var file_extension = path.parse(files.avatar.name).ext;
					//unique file name
					var unique_id = uniqid(file_name + file_extension.replace('.', '_'));
					//new file name with extension
					var new_filename = unique_id + file_extension;
					//renaming uploaded file

					fs.rename(files.avatar.path, path.join(__dirname, '../avatars/' + new_filename), function (err) {
						if (err)
							return res.send({ code: 0, resp: "Server error." });

						var prev_avatar = sanitize(req.user.avatar);
						User.update({ _id: id }, {
							$set: {
								nickname: sanitized_nickname,
								first_name: sanitized_first_name, surname: sanitized_surname,
								avatar: new_filename
							}
						}, function (err, result) {
							if (err) {
								fs.unlinkSync(file_name);
								return res.send({ code: 0, resp: "Server error." });
							}

							if (typeof prev_avatar !== 'undefined' && prev_avatar != "" && prev_avatar != "default_avatar.png") {
								fs.unlink(path.join(__dirname, '../avatars/' + prev_avatar), function (err) {
									if (err)
										return res.send({ code: 0, resp: "Server error." });

									return res.send({ code: 1, resp: "Profile updated" });
								});
							} else
								return res.send({ code: 1, resp: "Profile updated" });
						});

					});
				} else
					return res.send({ code: 0, resp: "File must have one of this extensions: jpg,jpeg,png." });
			}
		} else {
			if (fields.is_avatar_removed == "true" && typeof req.user.avatar !== 'unfefined') {
				User.update({ _id: id }, {
					$set: {
						nickname: sanitized_nickname,
						first_name: sanitized_first_name, surname: sanitized_surname, avatar: "default_avatar.png"
					}
				}, function (err, result) { //,$unset:{avatar:1}
					if (err)
						return res.send({ code: 0, resp: "Server error." });
					
					var prev_avatar = sanitize(req.user.avatar);

					if (typeof prev_avatar !== 'undefined' && prev_avatar != "" && prev_avatar != "default_avatar.png") {
						fs.unlink(path.join(__dirname, '../avatars/' + prev_avatar), function (err) {
							if (err)
								return res.send({ code: 0, resp: "Server error." });

							return res.send({ code: 1, resp: "Profile updated" });
						});
					} else
						return res.send({ code: 1, resp: "Profile updated" });
				});
			} else {
				User.update({ _id: id }, {
					$set: {
						nickname: sanitized_nickname,
						first_name: sanitized_first_name, surname: sanitized_surname
					}
				}, function (err, result) {
					if (err)
						return res.send({ code: 0, resp: "Server error." });

					res.send({ code: 1, resp: "Profile updated." });
				});
			}
		}
	});
});

///uploaded materials
router.get('/:user_id/uploaded_materials', MyAuth.ensureAuthenticated,MyAuth.checkIfIsOwner, function (req, res, next) {

	if (typeof req.params.user_id === 'string' && MyRegex.ObjectId_regex.test(req.params.user_id)) {

		var clear_user_id = sanitize(req.params.user_id);
		User.findById(clear_user_id, function (err, user) {
			if (err)
				return next(err);
			if (user) {

				var clear_user = {
					_id: xss(user._id),
					nickname: xss(user.nickname),
					is_owner: req.user.is_owner
				}

				Material.count({ "owner_id": clear_user_id }, function (err, c) {
					if (err)
						return next(err);

					res.render('user_materials', { materials_length: c, searched_user: clear_user });
				});
			} else {
				res.render('user_profile', { error_msg: "That user doesn't exist." });
			}
		});
	}
	else {
		res.render('user_profile', { error_msg: "That user doesn't exist." });
	}
});

router.get('/:user_id/uploaded_materials/:page', MyAuth.ensureAuthenticated, function (req, res, next) {

	if (typeof req.params.user_id === 'string' && MyRegex.ObjectId_regex.test(req.params.user_id)) {

		var user_id = sanitize(req.params.user_id);
		User.findById(user_id, function (err, user) {
			if (err)
				return next(err);

			if (user) {
				var page_num = req.params.page;
				var el_limit = 16;
				var parsed_page_num = parseInt(page_num, 10);

				if (page_num != parsed_page_num || parsed_page_num < 0) {
					page_num = 0;
				}

				var favourite_materials_ids = UniversalHelper.stringifyIds(req.user.favourite_materials_ids);

				Material.count({ "owner_id": user_id }, function (err, c) {
					if (err)
						return next(err);

					var max_pages = Math.floor(c / el_limit);    //floor because I am counting from 0
					if (page_num <= max_pages) {

						Material.find({ "owner_id": user_id }, {}, { sort: { "name": 1 }, limit: el_limit, skip: page_num * el_limit }, function (err, materials) {
							if (err)
								return next(err);

							var liked_materials_ids = UniversalHelper.stringifyIds(req.user.liked_materials_ids);

							MaterialHelper.prepareMaterials(materials, favourite_materials_ids, liked_materials_ids, function (err, result) {
								if (err)
									res.send({ code: 2, resp: "" });

								res.send({ code: 1, resp: result });
							});
						});
					} else {
						res.send({ code: 2, resp: "" });
					}
				});
			} else {
				res.send({ code: 2, resp: "" });
			}
		});
	} else {
		res.send({ code: 2, resp: "" });
	}
});

router.get('/:user_id/favourite', MyAuth.ensureAuthenticated, MyAuth.findUserRole,MyAuth.checkIfIsOwner, function (req, res, next) {

	if (typeof req.params.user_id === 'string' && MyRegex.ObjectId_regex.test(req.params.user_id)) {

		var clear_user_id = sanitize(req.params.user_id);
		User.findById(clear_user_id, function (err, searched_user) {
			if (err)
				return next(err);
			if (searched_user) {

				var favourite_materials_ids = searched_user.favourite_materials_ids;
				var c = 0;
				if (Array.isArray(favourite_materials_ids)) {
					c = favourite_materials_ids.length;
				}

				var is_admin = false;
				if (req.user.role_name == configEnv.admin_role)
					is_admin = true;

				var clear_user = {
					_id: xss(searched_user._id),
					nickname: xss(searched_user.nickname),
					show_favourites: searched_user.show_favourites ,
					is_owner: req.user.is_owner
				}

				res.render('favourite', { searched_user: clear_user, materials_length: c, admin: is_admin });

			} else {
				res.render('user_profile', { error_msg: "That user doesn't exist." });
			}
		});
	} else {
		res.render('user_profile', { error_msg: "That user doesn't exist." });
	}
});

router.get('/:user_id/favourite/:page', MyAuth.ensureAuthenticated, MyAuth.checkIfIsOwner, MyAuth.findUserRole, function (req, res, next) {

	if (typeof req.params.user_id === 'string' && MyRegex.ObjectId_regex.test(req.params.user_id)) {

		var clear_user_id = sanitize(req.params.user_id);
		User.findById(clear_user_id, function (err, user) {
			if (err)
				return next(err);
			if (user) {

				if (!user.show_favourites && !req.user.is_owner && req.user.role_name != configEnv.admin_role) {
					return res.send({ code: 2, resp: "" });
				}

				var page_num = req.params.page;
				var el_limit = 16;
				var parsed_page_num = parseInt(page_num, 10);

				if (page_num != parsed_page_num || parsed_page_num < 0) {
					page_num = 0;
				}

				var favourite_materials_ids = user.favourite_materials_ids;
				var fav_materials_length = 0;
				if (Array.isArray(favourite_materials_ids)) {	//typeof favourite_materials!=='undefined' && favourite_materials && 
					fav_materials_length = favourite_materials_ids.length;
				}
				var max_pages = Math.floor(fav_materials_length / el_limit);    //floor because I am counting from 0


				if (page_num <= max_pages) {

					Material.find({ "_id": { $in: favourite_materials_ids } }, {}, { sort: { "name": 1 }, limit: el_limit, skip: page_num * el_limit }, function (err, materials) {
						if (err)
							return next(err);

						var logged_user_liked_materials_ids = UniversalHelper.stringifyIds(req.user.liked_materials_ids);
						var logged_user_favourite_materials_ids = UniversalHelper.stringifyIds(req.user.favourite_materials_ids);

						MaterialHelper.prepareMaterials(materials, logged_user_favourite_materials_ids, logged_user_liked_materials_ids, function (err, result) {
							if (err)
								res.send({ code: 2, resp: "" });

							res.send({ code: 1, resp: result });
						});

					});
				} else {
					res.send({ code: 2, resp: "" });
				}
			} else {
				res.send({ code: 2, resp: "" });
			}
		});
	} else {
		res.send({ code: 2, resp: "" });
	}
});

router.post('/:user_id/favourite/edit_visibility', MyAuth.ensureAuthenticated, MyAuth.checkIfIsOwner, MyAuth.findUserRole, function (req, res, next) {

	if (req.user.is_owner == false && req.user.role_name != configEnv.admin_role)
		return res.send({ code: 0, resp: "You are not an owner!" });

	if (typeof req.params.user_id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.user_id))
		return res.send({ code: 0, resp: "Wrong user!" });

	if (typeof req.body.operation_type !== 'string' || (req.body.operation_type != "lock" && req.body.operation_type != "unlock"))
		return res.send({ code: 0, resp: "Wrong operation!" });

	var user_id = sanitize(req.params.user_id);

	User.findById(user_id, function (err, searched_user) {
		if (err)
			return res.send({ code: 0, resp: "Internal error!" });

		if (!searched_user)
			return res.send({ code: 0, resp: "Wrong user!" });

		var value = {};
		if (req.body.operation_type == "lock") {
			value.bool = false;
			value.text = "hidden";
		}
		else {
			value.bool = true;
			value.text = "visible"
		}

		searched_user.show_favourites = value.bool;

		searched_user.save(function (err) {
			if (err)
				return res.send({ code: 0, resp: "Internal error!" });
			else
				return res.send({ code: 1, resp: "Profile updated!<br>Favourite materials are now " + value.text + " !" });
		});
	});

});

router.post("/check_username", MyAuth.ensureNotAuthenticated, function (req, res, next) {

	var username = req.body.username;

	if (typeof username === 'string') {
		var username = sanitize(username);

		User.findOne({ "username": username }, function (err, result) {
			if (err)
				res.send({ code: 0, resp: "Internal error!" });
			if (result)
				res.send({ code: 0, resp: "This username is not unique!" });
			else
				res.send({ code: 1, resp: "Username OK!" });
		});
	} else {
		res.send({ code: 0, resp: "Wrong type!" });
	}
});


router.post("/check_email", MyAuth.ensureNotAuthenticated, function (req, res, next) {

	var email = req.body.email;

	if (typeof email === 'string') {
		var email = sanitize(email).toLowerCase();

		if (!validator.isEmail(email))
			return res.send({ code: 0, resp: "Email is not valid!" });

		User.findOne({ "email": email }, function (err, result) {
			if (err)
				res.send({ code: 0, resp: "Internal error!" });
			if (result)
				res.send({ code: 0, resp: "This email is not unique!" });
			else
				res.send({ code: 1, resp: "Email OK!" });
		});
	} else {
		res.send({ code: 0, resp: "Wrong type!" });
	}
});

module.exports = router;