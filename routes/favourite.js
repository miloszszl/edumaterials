var express = require('express');
var router = express.Router();
var User = require('../models/user');
var UniversalHelper = require('../helpers/UniversalHelper');
var sanitize = require('mongo-sanitize');
var Material = require('../models/material');
var MyRegex = require('../config/regex');

router.post('/:id/add', function (req, res, next) {

    if (typeof req.params.id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.id))
        return res.send({ code: 0, resp: "Invalid data." });

    var clear_material_id = sanitize(req.params.id);
    Material.findOne({ "_id": clear_material_id }, function (err, material) {
        if (err)
            return next(err);
        if (!material)
            return res.send({ code: 0, resp: "This material doesn't exist." });

        var favourite_materials_ids = UniversalHelper.stringifyIds(req.user.favourite_materials_ids);

        if (favourite_materials_ids.indexOf(JSON.stringify(req.params.id)) >= 0)
            return res.send({ code: 0, resp: "Material is already in favourite list." });

        req.user.favourite_materials_ids.push(clear_material_id);

        req.user.save(function (err) {
            if (err)
                return next(err);
            res.send({ code: 1, resp: "Material added to favourite." });
        });
    });
});

router.post('/:id/remove', function (req, res, next) {

    if (typeof req.params.id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.id))
        return res.send({ code: 0, resp: "Invalid data." });

    var clear_material_id = sanitize(req.params.id);
    Material.findOne({ "_id": clear_material_id }, function (err, material) {
        if (err)
            return next(err);
        if (!material)
            return res.send({ code: 0, resp: "This material doesn't exist." });

        var favourite_materials_ids = UniversalHelper.stringifyIds(req.user.favourite_materials_ids);

        var index = favourite_materials_ids.indexOf(JSON.stringify(clear_material_id));
        if (index < 0)
            return res.send({ code: 0, resp: "This material is not in your favourite list." });

        req.user.favourite_materials_ids.splice(index, 1);
        req.user.save(function (err) {
            if (err)
                return next(err);
            res.send({ code: 1, resp: "Material removed from favourite." });
        });
    });
});

module.exports = router;