var express = require('express');
var router = express.Router();
var path = require('path');
var formidable = require('formidable');
var fs = require('fs');
var uniqid = require('uniqid');
var Material = require('../models/material');
var mongoose = require('mongoose');
var validator = require('validator');
var User = require('../models/user');
var UniversalHelper = require('../helpers/UniversalHelper');
var MyAuth = require('../config/my_auth');
var xss = require('xss');
var sanitize = require('mongo-sanitize');
var MyRegex = require('../config/regex');
var Period = require('../models/period');
var PeriodDAO = require('../DAO/periodDAO');
var Subject = require('../models/subject');
var SubjectDAO = require('../DAO/subjectDAO');
var Category = require('../models/category');
var CategoryDAO = require('../DAO/categoryDAO');
var async = require('async');
var dateFormat = require('dateformat');
var MetadataParser = require('../helpers/MetadataParser');
var configEnv = require('../config/environment');
var EmailSender = require('../helpers/EmailSender');
var MaterialHelper = require('../helpers/MaterialHelper');
var SecurityHelper = require('../helpers/SecurityHelper');
var UserHelper=require('../helpers/UserHelper');

router.get("/:id/show", function (req, res, next) {
    if (typeof req.params.id === 'string' && MyRegex.ObjectId_regex.test(req.params.id)) {
        var clear_id = sanitize(req.params.id);
        Material.findOne({ "_id": clear_id }, function (err, material) {
            if (err)
                return next(err);
            if (material) {
                if (material.type == "application/pdf" || material.type.indexOf("image/") >= 0) {
                    var path = '../../' + material.relative_server_path;

                    res.render("material_preview", { material_name: xss(material.name), path: path, type: material.type, material_id: material._id });
                } else {
                    return res.render("material_preview", { error_msg: "This material can't be previewed." });
                }
            } else {
                return next(new Error('Wrong material.'));
            }
        });
    } else {
        var err = new Error('Wrong material.');
        err.status = 400;
        return next(err);
    }
});

//download file
router.get('/:id/download', function (req, res, next) {
    if (typeof req.params.id === 'string' && MyRegex.ObjectId_regex.test(req.params.id)) {
        var clear_id = sanitize(req.params.id);
        Material.findOneAndUpdate({ "_id": clear_id }, { $inc: { "num_of_downloads": 1 } }, { new: true }, function (err, material) {
            if (err)
                return next(err);
            
            if(!material){
                var err = new Error('Bad request');
                err.status = 400;
                return next(err);
            }

            var file = path.join(__dirname, '../learning_materials/' + material.relative_server_path);

            res.download(file, material.name);
        });
    } else {
        var err = new Error('Bad request');
        err.status = 400;
        return next(err);
    }
});

router.get('/periods', function (req, res, next) {
    Period.find({}, {}, { sort: { "name": 1 } }, function (err, periods) {
        if (err)
            return res.send({ code: 0, resp: "Internal server error" });

        var xss_clear_periods = SecurityHelper.clearArray(periods);
        return res.send({ code: 1, periods: xss_clear_periods });
    });
});

router.get('/upload', MyAuth.ensureAuthenticated, function (req, res, next) {
    async.waterfall([
        function (callback) {
            Period.find({}, {}, { sort: { "name": 1 } }, function (err, periods) {
                if (err)
                    return next(err)

                var xss_clear_periods = SecurityHelper.clearArray(periods);
                callback(err, xss_clear_periods);
            });
        }
    ],
        function (err, result) {
            if (err)
                return next(err);

            res.render('upload', { periods: result });
        });
});

router.get('/upload/:period_id/subjects_and_categories', function (req, res, next) {
    async.waterfall([
        function (callback) {
            if (typeof req.params.period_id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.period_id))
                return callback(new Error("Wrong period."))

            var period_id = sanitize(req.params.period_id)
            Period.findById(period_id, function (err, period) {
                if (err)
                    return callback(err);
                //period found
                if (period)
                    callback(null, period_id);
                else
                    callback(new Error("Wrong period."));
            });
        },
        function (period_id, callback) {
            Subject.find({ period_ids: period_id }, {}, { sort: { "name": 1 } }, function (err, subjects) {
                if (err)
                    return callback(err);

                //subjects found
                var subjects_length = subjects.length;
                for (var i = 0; i < subjects_length; i++)
                    subjects[i].name = xss(subjects[i].name);

                callback(null, period_id, subjects);
            });
        },
        function (period_id, subjects, callback) {
            Category.find({ period_ids: period_id }, {}, { sort: { "name": 1 } }, function (err, categories) {
                if (err)
                    return callback(err);
                //categories found
                var categories_length = categories.length;
                for (var i = 0; i < categories_length; i++)
                    categories[i].naxe = xss(categories[i].name);

                callback(null, subjects, categories);
            });
        }
    ],
        function (err, subjects, categories) {
            if (err)
                return res.send({ code: 0, resp: err.message });

            res.send({ code: 1, subjects: subjects, categories: categories });
        });
});

router.post('/subjects_and_categories', function (req, res, next) {
    async.waterfall([
        function (callback) {
            var period_ids = req.body["period_ids[]"];

            if (typeof period_ids !== 'string' && typeof period_ids !== 'object')
                return callback(new Error("Wrong periods."))

            if (typeof period_ids === 'string')
                period_ids = [period_ids];

            var period_ids_len = period_ids.length;

            for (var i = 0; i < period_ids_len; i++) {
                if (!MyRegex.ObjectId_regex.test(period_ids[i])) {
                    return callback(new Error("Wrong periods."))
                } else {
                    period_ids[i] = sanitize([period_ids[i]]);
                }
            }

            Period.count({ "_id": { $in: period_ids } }, function (err, c) {
                if (err)
                    return callback(err);
                //period found
                if (c = period_ids_len)
                    callback(null, period_ids);
                else
                    callback(new Error("Wrong periods."));
            });
        },
        function (period_ids, callback) {
            Subject.find({ "period_ids": { $in: period_ids } }, {}, { sort: { "name": 1 } }, function (err, subjects) {
                if (err)
                    return callback(err);

                //subjects found
                var subjects_length = subjects.length;
                for (var i = 0; i < subjects_length; i++)
                    subjects[i].name = xss(subjects[i].name);

                callback(null, period_ids, subjects);
            });
        },
        function (period_ids, subjects, callback) {
            Category.find({ "period_ids": { $in: period_ids } }, {}, { sort: { "name": 1 } }, function (err, categories) {
                if (err)
                    return callback(err);
                //categories found
                var categories_length = categories.length;
                for (var i = 0; i < categories_length; i++)
                    categories[i].naxe = xss(categories[i].name);

                callback(null, subjects, categories);
            });
        }
    ],
        function (err, subjects, categories) {
            if (err)
                return res.send({ code: 0, resp: err.message });

            res.send({ code: 1, subjects: subjects, categories: categories });
        });
});

router.post('/upload2', MyAuth.ensureAuthenticated, function (req, res, next) {
    var form = new formidable.IncomingForm();
    form.multiples = false;

    form.parse(req, function (err, fields, files) {
        async.waterfall([
            function (callback) {

                if (typeof files !== 'object')
                    return callback(new Error("Wrong file type!"));

                if (typeof fields !== 'object')
                    return callback(new Error("Wrong fields values!"));

                if (typeof fields.period_name !== 'string' || fields.period_name.length <= 0)
                    return callback(new Error("Wrong period name!"));

                if (typeof fields.subject_name !== 'string' || fields.subject_name.length <= 0)
                    return callback(new Error("Wrong subject name!"));

                if (typeof fields.category_name !== 'string' || fields.category_name.length <= 0)
                    return callback(new Error("Wrong category name!"));

                if (typeof fields.description !== 'string' || fields.description.length > 400)
                    return callback(new Error("Wrong description length!"));

                callback(null);
            },
            function (callback) {
                var full_metadata = {};
                PeriodDAO.findByNameRegexCaseInsensitive(fields.period_name, full_metadata, callback);
            },
            function (full_metadata, callback) {
                SubjectDAO.findByNameRegexCaseInsensitive(fields.subject_name, full_metadata, callback)
            },
            function (full_metadata, callback) {
                CategoryDAO.findByNameRegexCaseInsensitive(fields.category_name, full_metadata, callback)
            },
            function (full_metadata, callback) {

                var result = {
                    period_id: full_metadata.period._id,
                    period_name: full_metadata.period.name,
                    subject_id: full_metadata.subject._id,
                    subject_name: full_metadata.subject.name,
                    category_id: full_metadata.category._id,
                    category_name: full_metadata.category.name,
                    description: sanitize(fields.description.trim())
                }

                callback(null, result);
            }, function (result, callback) {
                MaterialHelper.saveFile(req.user, result, files, callback);
            }],
            function (err, user_free_disk_space, metadata) {
                if (err) {
                    return res.send({ code: 0, resp: err.message });
                }

                res.send({
                    code: 1, resp: "Success! Material uploaded. <br>Period: " + metadata.period_name + "<br>Subject: " + metadata.subject_name + "<br>Category: " + metadata.category_name,
                    user_free_disk_space: user_free_disk_space
                });
            });
    });
});

router.post('/upload', MyAuth.ensureAuthenticated, function (req, res, next) {

    var form = new formidable.IncomingForm();
    form.multiples = false;

    form.parse(req, function (err, fields, files) {
        async.waterfall([
            function (callback) {
                if (typeof fields === 'undefined' || !fields)
                    return callback(new Error("Wrong fields."))

                callback(null);
            },
            function (callback) {
                if (typeof fields.period_id !== 'string' || !MyRegex.ObjectId_regex.test(fields.period_id))
                    return callback(new Error("Wrong period."));

                var period_id = sanitize(fields.period_id);
                Period.findById(period_id, function (err, period) {
                    if (err)
                        return callback(err);

                    if (period)
                        callback(null, period_id);
                    else
                        callback(new Error("Wrong period."));
                });

            },
            function (period_id, callback) {
                if (typeof fields.subject_id !== 'string' || !MyRegex.ObjectId_regex.test(fields.subject_id))
                    return callback(new Error("Wrong subject."));

                var subject_id = sanitize(fields.subject_id);
                Subject.findOne({ "_id": subject_id, period_ids: period_id }, function (err, subject) {
                    if (err)
                        return callback(new Error("Server Error."));

                    if (subject)
                        callback(null, period_id, subject_id);
                    else
                        callback(new Error("Wrong subject."));
                });

            },
            function (period_id, subject_id, callback) {
                if (typeof fields.category_id !== 'string' || !MyRegex.ObjectId_regex.test(fields.category_id))
                    return callback(new Error("Wrong category"));

                var category_id = sanitize(fields.category_id);
                Category.findOne({ "_id": category_id, period_ids: period_id }, function (err, category) {
                    if (err)
                        return callback(new Error("Server Error."));

                    if (category)
                        callback(null, period_id, subject_id, category_id);
                    else
                        callback(new Error("Wrong category."));
                });

            },
            function (period_id, subject_id, category_id, callback) {
                if (typeof fields.description !== 'string' || fields.description.length > 400)
                    return callback(new Error("Description is too long!"));

                var description = sanitize(fields.description);
                callback(null, period_id, subject_id, category_id, description);

            },
            function (period_id, subject_id, category_id, description, callback) {
                var result = {
                    period_id: period_id,
                    subject_id: subject_id,
                    category_id: category_id,
                    description: description
                }

                callback(null, result);
            }, function (result, callback) {
                MaterialHelper.saveFile(req.user, result, files, callback);
            }
        ], function (err, result) {
            if (err) {
                return res.send({ code: 0, resp: err.message });
            }

            res.send({ code: 1, resp: "Success! Material uploaded.", user_free_disk_space: result });
        });
    });
});

router.post('/:id/like', MyAuth.ensureAuthenticatedAjax, function (req, res, next) {

    if (typeof req.params.id === 'string' && MyRegex.ObjectId_regex.test(req.params.id)) {
        var clear_material_id = sanitize(req.params.id);

        Material.findOne({ "_id": clear_material_id }, function (err, material) {
            if (err)
                return next(err);
            if (material) {

                var liked_materials_ids = UniversalHelper.stringifyIds(req.user.liked_materials_ids);

                if (liked_materials_ids.indexOf(JSON.stringify(req.params.id)) >= 0) {
                    res.send({ code: 0, resp: "You like this material already." });
                } else {

                    req.user.liked_materials_ids.push(clear_material_id);

                    req.user.save(function (err) {
                        if (err)
                            return next(err);

                        Material.update({ "_id": clear_material_id }, { $inc: { "num_of_likes": 1 } }, { new: true }, function (err) {
                            if (err)
                                return next(err);
                            res.send({ code: 1, resp: "Material liked. " });
                        });
                    });
                }
            } else {
                res.send({ code: 0, resp: "This material doesn't exist." });
            }
        });
    } else {
        res.send({ code: 0, resp: "Invalid data." });
    }
});


router.post('/:id/remove_like', MyAuth.ensureAuthenticatedAjax, function (req, res, next) {

    if (typeof req.params.id === 'string' && MyRegex.ObjectId_regex.test(req.params.id)) {
        var clear_material_id = sanitize(req.params.id);

        Material.findOne({ "_id": clear_material_id }, function (err, material) {
            if (err)
                return next(err);
            if (material) {

                var liked_materials_ids = UniversalHelper.stringifyIds(req.user.liked_materials_ids);

                var index = liked_materials_ids.indexOf(JSON.stringify(clear_material_id));
                if (index >= 0) {

                    req.user.liked_materials_ids.splice(index, 1);

                    req.user.save(function (err) {
                        if (err)
                            return next(err);

                        Material.update({ "_id": clear_material_id }, { $inc: { "num_of_likes": -1 } }, { new: true }, function (err) {
                            if (err)
                                return next(err);
                            res.send({ code: 1, resp: "You don't like this material." });
                        });
                    });
                } else {
                    res.send({ code: 0, resp: "You don't like this material already." });
                }

            } else {
                res.send({ code: 0, resp: "This material doesn't exist." });
            }
        });

    } else {
        res.send({ code: 0, resp: "Invalid data." });
    }
});


router.get('/:id', MyAuth.checkIfIsOwner, MyAuth.findUserRole, function (req, res, next) {
    async.waterfall([
        function (callback) {
            if (typeof req.params.id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.id))
                return callback(new Error("Wrong material"));

            var clear_material_id = sanitize(req.params.id);
            Material.findOne({ "_id": clear_material_id }, function (err, material) {
                if (err)
                    return callback(err);
                if (material) {

                    var upload_date = new Date(material.upload_date);
                    var full_upload_date = dateFormat(upload_date, "dd-mm-yyyy, hh:MM:ss TT")
                    var units = MaterialHelper.calculateUnits(material);

                    var clear_comments = [];
                    var loop_ok = true;
                    var loop_err;
                    async.eachSeries(material.comments, function (item, loop_callback) {
                        User.findById(item.author_id, function (err, searched_user) {
                            if (err)
                                return loop_callback(err);
                            if (searched_user) {
                                clear_comments.push({
                                    text: xss(item.text), author_id: searched_user._id, author_nickname: searched_user.nickname, author_avatar: searched_user.avatar,
                                    // publish_date:item.publish_date});
                                    publish_date: dateFormat(new Date(item.publish_date), "dd-mm-yyyy, hh:MM:ss TT")
                                });
                                loop_callback();
                            } else
                                loop_callback(new Error("User not found."));
                        });
                    },
                        function (err) {
                            if (err)
                                return callback(err);

                            clear_comments.sort(function (a, b) {
                                return new Date(a.publish_date) - new Date(b.publish_date);
                            });

                            var preview = false;
                            if (material.type == "application/pdf" || material.type.indexOf("image/") >= 0) {
                                preview = true;
                            }

                            var my_material = {
                                _id: material._id,
                                name: xss(material.name),
                                description: xss(material.description),
                                upload_date: full_upload_date,
                                owner_id: material.owner_id,
                                owner_nickname: "",
                                owner_avatar: "",
                                type: material.type,
                                extension: material.extension,
                                num_of_downloads: material.num_of_downloads,
                                num_of_likes: material.num_of_likes,
                                units: units,
                                size: material.size.toFixed(2),
                                is_liked: "",
                                is_favourite: "",
                                comments: clear_comments,
                                num_of_comments: material.comments.length,
                                preview: preview,
                                period_id: material.period_id,
                                subject_id: material.subject_id,
                                category_id: material.category_id,
                            }

                            callback(null, my_material)
                        });
                } else {
                    return callback(new Error("Wrong material."));
                }
            });
        },
        function (material, callback) {
            var user_id = sanitize(material.owner_id);
            User.findById(user_id, function (err, searched_user) {
                if (err)
                    return callback(err);
                if (searched_user) {
                    material.owner_nickname = xss(searched_user.nickname);
                    material.owner_avatar = xss(searched_user.avatar);

                    if (MyAuth.checkIfUserIsAuthenticated(req)) {
                        material.logged_in = true;

                        if (req.user._id.equals(user_id))
                            material.is_owner = true;
                        else
                            material.is_owner = false;

                        var is_admin = false;
                        if (req.user && (req.user.role_name == configEnv.admin_role))
                            is_admin = true;

                        material.is_admin = is_admin;

                        var favourite_materials_ids = UniversalHelper.stringifyIds(req.user.favourite_materials_ids);
                        var liked_materials_ids = UniversalHelper.stringifyIds(req.user.liked_materials_ids);
                        material.is_favourite = UniversalHelper.checkIfElementIsInArray(favourite_materials_ids, material._id);
                        material.is_liked = UniversalHelper.checkIfElementIsInArray(liked_materials_ids, material._id);
                    } else {
                        material.is_favourite = false;
                        material.is_liked = false;
                    }

                    callback(null, material)
                }
                else
                    callback(new Error("User not found"));
            });
        },
        function (material, callback) {
            Period.findById(material.period_id, function (err, result) {
                if (err)
                    return callback(err);

                if (result) {
                    material.period_name = result.name;
                    callback(null, material)
                } else {
                    return callback(new Error("Internal error!"));
                }
            });
        },
        function (material, callback) {
            Subject.findById(material.subject_id, function (err, result) {
                if (err)
                    return callback(err);

                if (result) {
                    material.subject_name = result.name;
                    callback(null, material)
                } else {
                    return callback(new Error("Internal error!"));
                }
            });
        },
        function (material, callback) {
            Category.findById(material.category_id, function (err, result) {
                if (err)
                    return callback(err);

                if (result) {
                    material.category_name = result.name;
                    callback(null, material)
                } else {
                    return callback(new Error("Internal error!"));
                }
            });
        }
    ], function (err, result) {
        if (err)
            return res.render("material_details", { error_msg: err.message });

        res.render("material_details", { material: result });
    });

});

router.post('/:id/comment', MyAuth.ensureAuthenticatedAjax, function (req, res, next) {

    async.waterfall([
        function (callback) {
            if (typeof req.params.id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.id))
                return callback(new Error("Wrong material."));

            if (typeof req.body.text !== 'string' || req.body.text.length > 800 || req.body.text.length < 1)
                return callback(new Error("Comment should be 1-800 characters long."));

            var material_id = sanitize(req.params.id);
            Material.findById(material_id, function (err, material) {
                if (err)
                    return callback(err);
                if (material) {

                    var comment = {
                        _id: mongoose.Types.ObjectId(),
                        text: sanitize(req.body.text),
                        author_id: req.user._id,
                        publish_date: new Date()
                    }

                    material.comments.push(comment);

                    material.save(function (err, material) {
                        if (err)
                            return callback(err);

                        comment.author_nickname = xss(req.user.nickname);
                        comment.author_avatar = xss(req.user.avatar);
                        comment.text = xss(comment.text);
                        comment.publish_date = dateFormat(new Date(comment.publish_date), "dd-mm-yyyy, hh:MM:ss TT");

                        callback(null, comment)
                    });
                } else
                    callback(new Error("Wrong material."));
            });
        }
    ], function (err, result) {
        if (err) {
            return res.send({ code: 0, resp: err.message });
        }

        res.send({ code: 1, resp: "Comment added.", comment: result });
    });
});

router.post('/:id/report', MyAuth.ensureAuthenticated, function (req, res, next) {

    if (typeof req.params.id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.id))
        return res.send({ code: 0, resp: "Wrong material!" });
    if (typeof req.body.details !== 'string' || req.body.details.length > 400)
        return res.send({ code: 0, resp: "Incorrect description!" });

    var material_id = sanitize(req.params.id);
    Material.findById(material_id, function (err, searched_material) {
        if (err)
            return next(err);
        if (!searched_material)
            return res.send({ code: 0, resp: "Wrong material!" });

        var user_id = req.user._id;
        var user_nickname = (typeof req.user.nickname === 'undefined' || req.user.nickname == null) ? "" : req.user.nickname;
        var user_email = (typeof req.user.email === 'undefined' || req.user.email == null) ? "" : req.user.email;
        var material_name = searched_material.name;
        var material_id = searched_material._id;

        var transporter = EmailSender.createTransporter();

        var mailOptions = {
            from: '"' + user_nickname + '" <' + user_email + '>',
            to: 'miloszszlachetka@gmail.com',
            subject: "Report",
            text: 'Material id: ' + material_id + '\n' + 'Material name: ' + material_name + '\n' +
                'User ID: ' + user_id + '\nDeteils: ' + req.body.details
        };

        if (req.app.get('env') === 'development')
            process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

        transporter.sendMail(mailOptions, function (err) {
            if (err)
                return res.send({ code: 0, resp: "Internal error!" });

            return res.send({ code: 1, resp: "Material has been reported.<br>Thank you!" })
        });
    });
});

router.delete("/:id", MyAuth.ensureAuthenticated, MyAuth.checkIfIsOwner, function (req, res, next) {

    if (!req.user || req.user.is_owner == false || (req.user.role_name != configEnv.admin_role)) {
        res.send({ code: 0, resp: "You are not the owner!" });
    } else {
        if (typeof req.params.id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.id))
            return res.send({ code: 0, resp: "Wrong material!" });

        var material_id = sanitize(req.params.id);

        Material.findById(material_id, function (err, searched_material) {
            if (err)
                return res.send({ code: 0, resp: "Internal error!" });

            if (searched_material) {
                req.user.free_disk_space = req.user.free_disk_space + searched_material.size / (1024 * 1024);
                req.user.save(function (err) {
                    if (err)
                        return res.send({ code: 0, resp: "Internal error!" });
                    User.update({}, { $pull: { "favourite_materials_ids": searched_material._id, "liked_materials_ids": searched_material._id } }, function (err) {
                        if (err)
                            return res.send({ code: 0, resp: "Internal error!" });

                        var my_path = path.join(__dirname, '../learning_materials');
                        var my_full_path = path.join(my_path, searched_material.relative_server_path);
                        fs.unlink(my_full_path, function (err) {
                            if (err)
                                return res.send({ code: 0, resp: "Internal error!" });

                            Material.remove({ "_id": searched_material._id }, function (err) {
                                if (err)
                                    return res.send({ code: 0, resp: "Internal error!" });

                                req.flash('success_msg', "Material deleted!");
                                return res.send({ code: 1 });
                            });
                        });
                    });
                });
            } else {
                return res.send({ code: 0, resp: "Wrong material!" });
            }
        });
    }
});

router.patch("/:id", MyAuth.ensureAuthenticated, MyAuth.checkIfIsOwner, function (req, res, next) {
    if (req.user.is_owner == false) {
        res.send({ code: 0, resp: "You are not the owner!" });
    } else {
        if (typeof req.params.id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.id))
            return res.send({ code: 0, resp: "Wrong material!" });

        var material_id = sanitize(req.params.id);

        Material.findById(material_id, function (err, searched_material) {
            if (err)
                return res.send({ code: 0, resp: "Internal error!" });

            if (searched_material) {
                if (typeof req.body.desc === 'string' && req.body.desc.length <= 400) {
                    var clear_description = sanitize(req.body.desc);
                    searched_material.description = clear_description;
                    searched_material.save(function (err) {
                        if (err)
                            return res.send({ code: 0, resp: "Internal error!" });

                        return res.send({ code: 1, resp: "Description updated!", description: xss(clear_description) });
                    });
                } else {
                    return res.send({ code: 0, resp: "Invalid description!" });
                }
            } else {
                return res.send({ code: 0, resp: "Wrong material!" });
            }
        });
    }
});

router.get("/:id/recommended", function (req, res, next) {

    if (typeof req.params.id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.id))
        return res.send({ code: 0, resp: "Wrong material." });

    if (typeof req.cookies !== 'object' || typeof req.cookies.user_choices !== 'string')
        return res.send({ code: 0, resp: "Wrong material." });

    var arr = JSON.parse(req.cookies.user_choices);

    MaterialHelper.findRecommendedMaterials(req.params.id,arr,function (err, result) {
        if (err)
            return res.send({ code: 0, resp: err.message });

        if (result.length == 0)
            return res.send({ code: 1, resp: "No recommended materials!" });
        

        var user_materials = UserHelper.getLikedAndFavouriteMaterialsIds(req);
        var favourite_materials_ids = user_materials.favourite_materials_ids;
        var liked_materials_ids = user_materials.liked_materials_ids;

        MaterialHelper.prepareMaterials(result, favourite_materials_ids, liked_materials_ids, function (err, result) {
            if (err)
                return res.send({ code: 0, resp: err.message });

            res.send({ code: 1, recommended_materials: result });
        });
    });
});

function validateSearchCriteria(val) {
    if (typeof val === 'string')
        if (MyRegex.ObjectId_regex.test(val))
            return [mongoose.Types.ObjectId(val)];
        else
            return false;

    if (typeof val === 'undefined')
        return val

    if (val.constructor === Array) {
        if (!SecurityHelper.areArrayElementsStringAndObjectId(val)) {
            return false;
        }
        else {
            return SecurityHelper.sanitizeIds(val);
        }
    }
    return false;
}

function validateSortOption(sort_opt) {
    if (typeof sort_opt === 'string' && (sort_opt == "az" || sort_opt == "za" || sort_opt == "m_popular" || sort_opt == "l_popular"))
        return sort_opt
    return false
}

function getSortOption(sort_option) {
    switch (sort_option) {
        case "az":
            return { "insensitive": 1 }
            break;

        case "za":
            return { "insensitive": -1 }
            break;

        case "m_popular":
            return { "count": -1 }
            break;

        default:
            return { "count": 1 }
    }
}

function generateSearchCriteria(period_ids, subject_ids, category_ids) {
    var match_criteria = {};
    if (period_ids && period_ids.constructor === Array)
        match_criteria["period_id"] = { $in: period_ids };
    if (subject_ids && subject_ids.constructor === Array)
        match_criteria["subject_id"] = { $in: subject_ids };
    if (category_ids && category_ids.constructor === Array)
        match_criteria["category_id"] = { $in: category_ids };

    return match_criteria;
}

function inviniteScroll(page_num, match_criteria, sort_opt, callback) {

    var el_limit = 12;
    var parsed_page_num = parseInt(page_num, 10);

    if (page_num != parsed_page_num || parsed_page_num < 0) {
        page_num = 0;
    }

    Material.count(match_criteria, function (err, c) {
        if (err)
            return next(err);

        var max_pages = Math.floor(c / el_limit);    //floor because I am counting from 0

        if (page_num <= max_pages) {

            Material.aggregate([
                {
                    $match: match_criteria
                },
                {
                    $project: {
                        name: 1,
                        extension: 1,
                        relative_server_path: 1,
                        owner_id: 1,
                        size: 1,
                        type: 1,
                        num_of_downloads: 1,
                        num_of_likes: 1,
                        period_id: 1,
                        subject_id: 1,
                        category_id: 1,
                        insensitive: { "$toLower": "$name" },
                        count: { $add: ["$num_of_likes", "$num_of_downloads", { $size: "$comments" }] }
                    }
                },
                { $sort: sort_opt },
                { $skip: page_num * el_limit },
                { $limit: el_limit },
            ], function (err, materials) {
                if (err)
                    return callback(err);

                callback(null, materials, page_num);
            });
        } else
            return callback(null, "", -1);
    });
}

function generateMainPageAjaxResponse(err, result, page_num, req, res) {
    if (err)
        return res.send({ code: 0, resp: err.message });
    if (page_num == -1)
        return res.send({ code: 2, resp: "" });

    var user_materials = UserHelper.getLikedAndFavouriteMaterialsIds(req);
    var favourite_materials_ids = user_materials.favourite_materials_ids;
    var liked_materials_ids = user_materials.liked_materials_ids;

    MaterialHelper.prepareMaterials(result, favourite_materials_ids, liked_materials_ids, function (err, result) {
        if (err)
            return res.send({ code: 0, resp: err.message });

        if (page_num == 0)
            res.send({ code: 3, resp: result });
        else
            res.send({ code: 1, resp: result });
    });
}

router.post('/find_by_criteria/:page', function (req, res, next) {
    async.waterfall([
        function (callback) {

            var sort_option = validateSortOption(req.body.sort_option);
            var period_ids = validateSearchCriteria(req.body["period_ids[]"]);
            var subject_ids = validateSearchCriteria(req.body["subject_ids[]"]);
            var category_ids = validateSearchCriteria(req.body["category_ids[]"]);

            if (sort_option == false || period_ids == false || subject_ids == false || category_ids == false)
                return callback(new Error("Wrong values"));

            callback(null, period_ids, subject_ids, category_ids, sort_option);
        },
        function (period_ids, subject_ids, category_ids, sort_option, callback) {

            var sort_opt = getSortOption(sort_option);

            var match_criteria = generateSearchCriteria(period_ids, subject_ids, category_ids);

            //infinite scroll
            inviniteScroll(req.params.page, match_criteria, sort_opt, callback);
        }
    ],
        function (err, result, page_num) {
            generateMainPageAjaxResponse(err, result, page_num, req, res);
        });
});

router.get("/autocomplete/periods", function (req, res, next) {
    if (typeof req.query.term !== 'string')
        return res.send(JSON.stringify([]));

    var query = sanitize(req.query.term);
    var metadata_object = MetadataParser.createRegexObject(query);

    PeriodDAO.findManyByNameRegexCaseInsensitive(metadata_object, function (err, results_arr) {
        if (err)
            return res.send(JSON.stringify([]));

        res.send(JSON.stringify(results_arr));
    });
});

router.get("/autocomplete/subjects", function (req, res, next) {

    if (typeof req.query.period_name !== 'string' || typeof req.query.subject_name !== 'string')
        return res.send(JSON.stringify([]));

    var period_name = sanitize(req.query.period_name);
    var subject_name = sanitize(req.query.subject_name);

    PeriodDAO.findIdByNameCaseInsensitive(period_name, function (err, period_id) {
        if (err)
            return res.send(JSON.stringify([]));

        var metadata_object = MetadataParser.createRegexObject(subject_name);
        SubjectDAO.findManyByNameRegexCaseInsensitive(period_id, metadata_object, function (err, subjects) {
            if (err)
                return res.send(JSON.stringify([]));

            res.send(JSON.stringify(subjects));
        });
    });
});

router.get("/autocomplete/categories", function (req, res, next) {

    if (typeof req.query.period_name !== 'string' || typeof req.query.category_name !== 'string')
        return res.send(JSON.stringify([]));

    var period_name = sanitize(req.query.period_name);
    var category_name = sanitize(req.query.category_name);

    PeriodDAO.findIdByNameCaseInsensitive(period_name, function (err, period_id) {
        if (err)
            return res.send(JSON.stringify([]));

        var metadata_object = MetadataParser.createRegexObject(category_name);
        CategoryDAO.findManyByNameRegexCaseInsensitive(period_id, metadata_object, function (err, categories) {
            if (err)
                return res.send(JSON.stringify([]));

            res.send(JSON.stringify(categories));
        });
    });
});

module.exports = router;