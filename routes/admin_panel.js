var express = require('express');
var router = express.Router();
var Category = require('../models/category');
var Period = require('../models/period');
var Subject = require('../models/subject');
var User = require('../models/user');
var validator = require('validator');
var xss = require('xss');
var sanitize = require('mongo-sanitize');
var Role = require('../models/role');
var MyRegex = require('../config/regex');
var async = require('async');
var SecurityHelper = require('../helpers/SecurityHelper');
var RoleHelper = require('../helpers/RoleHelper');
var UniversalHelper = require('../helpers/UniversalHelper');

router.get('/', function (req, res) {
    res.render('admin_panel');
});

router.get('/categories', function (req, res, next) {
    Category.find({}, {}, { sort: { name: 1 } }, function (err, categories) {       //select all and sort by name ASC
        if (err)
            return next(err);

        SecurityHelper.xssClearNamesInArray(categories);

        Period.find({}, {}, { sort: { "name": 1 } }, function (err, periods) {
            if (err)
                return next(err);

            SecurityHelper.xssClearNamesInArray(periods);

            res.render('admin_categories', { categories: categories, periods: periods });
        });
    });
});

router.post('/categories/add', function (req, res, next) {
    async.waterfall([
        function (callback) {

            if (typeof req.body.new_category_name !== 'string' || !validator.isLength(req.body.new_category_name, { min: 1, max: 200 }))
                return callback(new Error("Category name should be 1-200 characters long"));

            if (typeof req.body.period_ids === "string")   //one selected
                req.body.period_ids = [req.body.period_ids];
            else if (!(Array.isArray(req.body.period_ids) && req.body.period_ids.length > 0 && SecurityHelper.areArrayElementsStringAndObjectId(req.body.period_ids)))
                return callback(new Error("Wrong periods."));

            var category_name = sanitize(req.body.new_category_name);

            Category.findOne({ "name": category_name }, function (err, category) {
                if (err)
                    return callback(err);
                if (category)
                    return callback(new Error("This category already exists."));
                else
                    callback(null, category_name);
            });
        },
        function (category_name, callback) {

            var period_ids = SecurityHelper.uniqueElements(req.body.period_ids);

            if(!SecurityHelper.areArrayElementsStringAndObjectId(period_ids))
                return callback(new Error("Wrong periods."));

            Period.count({ "_id": { $in: period_ids } }, function (err, c) {
                if (err)
                    return next(err);

                if (c == period_ids.length)
                    callback(null, category_name, period_ids);
                else
                    callback(new Error("Wrong periods."));
            });
        },
        function (category_name, period_ids, callback) {

            var category = new Category({
                name: category_name,
                period_ids: period_ids
            });

            category.save(function (err, new_category) {
                if (err)
                    return callback(err);

                Period.find({}, {}, { sort: { "name": 1 } }, function (err, all_periods) {  //TODO
                    if (err)
                        return callback(err);

                    SecurityHelper.xssClearNamesInArray(all_periods);

                    var result = {
                        name: xss(new_category.name),
                        _id: new_category._id,
                        period_ids: new_category.period_ids,
                        periods: all_periods
                    }
                    callback(null, result);
                });
            });
        }
    ],
        function (err, result) {
            if (err)
                return res.send({ code: 0, resp: err.message });

            res.send({ code: 1, resp: "Category " + result.name + " created.", result: result });
        });
});

router.post('/categories/delete', function (req, res, next) {
    async.waterfall([
        function (callback) {

            if (typeof req.body.category_id !== 'string' || !MyRegex.ObjectId_regex.test(req.body.category_id))
                return callback(new Error("Wrong category."));

            var category_id = sanitize(req.body.category_id);
            Category.findOneAndRemove({ "_id": category_id }, function (err, category) {
                if (err)
                    return callback(err);
                if (category)
                    callback(null, xss(category.name));
                else
                    callback(new Error("This category doesn't exist."));

            });
        }
    ], function (err, result) {
        if (err)
            return res.send({ code: 0, resp: err.message });

        res.send({ code: 1, resp: "Category " + result + " deleted." });
    });
});

router.post('/categories/update', function (req, res, next) {
    async.waterfall([
        function (callback) {

            if (typeof req.body.category_id !== 'string' || !MyRegex.ObjectId_regex.test(req.body.category_id))
                return callback(new Error("Wrong category."));

            if (typeof req.body.name !== 'string' || !validator.isLength(req.body.name, { min: 1, max: 200 }))
                return callback(new Error("Category name should be 1-200 characters long."));

            if (typeof req.body["period_ids[]"] === "string")
                req.body["period_ids[]"] = [req.body["period_ids[]"]];
            if (!(Array.isArray(req.body["period_ids[]"]) && req.body["period_ids[]"].length > 0 && SecurityHelper.areArrayElementsStringAndObjectId(req.body["period_ids[]"])))
                return callback(new Error("Select periods."));

            var category_id = sanitize(req.body.category_id);
            Category.findById(category_id, function (err, category) {
                if (err)
                    return callback(err);
                if (category)
                    callback(null, category, category_id);
                else
                    callback(new Error("Wrong category."));
            });
        },
        function (category, category_id, callback) {

            var category_name = sanitize(req.body.name);
            Category.findOne({ "name": category_name, "_id": { $ne: category_id } }, function (err, second_category) {
                if (err)
                    return callback(err);
                if (second_category)
                    callback(new Error("This category already exists."));
                else {
                    category.name = category_name;
                    callback(null, category);
                }
            });
        },
        function (category, callback) {

            var period_ids = SecurityHelper.uniqueElements(req.body["period_ids[]"]);
            
            if(!SecurityHelper.areArrayElementsStringAndObjectId(period_ids))
                return callback(new Error("Wrong periods."));

            Period.count({ "_id": { $in: period_ids } }, function (err, c) {
                if (err)
                    return next(err);
                if (c == period_ids.length) {
                    category.period_ids = period_ids;
                    callback(null, category);
                }
                else
                    callback(new Error("Wrong periods."));
            });
        },
        function (category, callback) {
            category.save(function (err) {
                if (err)
                    return callback(err);

                category.name = xss(category.name);

                var result = {
                    category: category
                };

                callback(null, result);
            });
        }
    ], function (err, result) {
        if (err)
            return res.send({ code: 0, resp: err.message });

        res.send({ code: 1, resp: "Category " + result.category.name + " updated.", result: result });
    });
});

router.get('/periods', function (req, res, next) {
    Period.find({}, 'name', { sort: { name: 1 } }, function (err, periods) {
        if (err)
            return next(err);

        SecurityHelper.xssClearNamesInArray(periods);

        res.render('admin_periods', { periods: periods });
    });
});

router.post('/periods/add', function (req, res, next) {
    async.waterfall([
        function (callback) {
            if (typeof req.body.new_period_name !== 'string' || !validator.isLength(req.body.new_period_name, { min: 1, max: 200 }))
                return callback(new Error("Period name should be 1-200 characters long."))

            var period_name = sanitize(req.body.new_period_name);
            Period.findOne({ "name": period_name }, function (err, period) {
                if (err)
                    return callback(err);
                if (period)
                    callback(new Error("This period already exists."));
                else
                    callback(null, period_name);
            });
        },
        function (period_name, callback) {
            var period = new Period({
                name: period_name
            });

            period.save(function (err, result) {
                if (err)
                    return callback(err);

                var result = {
                    name: xss(result.name),
                    _id: result._id
                }
                callback(null, result)
            });
        }
    ],
        function (err, result) {
            if (err)
                return res.send({ code: 0, resp: err.message });

            res.send({ code: 1, resp: "Period " + result.name + " created.", result: result })
        });
});

router.post('/periods/delete', function (req, res, next) {
    if (typeof req.body.period_id !== 'string' || !MyRegex.ObjectId_regex.test(req.body.period_id))
        return res.send({ code: 0, resp: "Wrong period." });

    var period_id = sanitize(req.body.period_id);
    Period.findOneAndRemove({ "_id": period_id }, function (err, period) {
        if (err)
            return res.send({ code: 0, resp: err.message });
        if (period)
            res.send({ code: 1, resp: "Period " + xss(period.name) + " has been removed." });
        else
            res.send({ code: 0, resp: "This period doesn't exist." });
    });
});

router.post('/periods/update', function (req, res, next) {
    async.waterfall([
        function (callback) {
            if (typeof req.body.period_id !== 'string' || !MyRegex.ObjectId_regex.test(req.body.period_id))
                return callback(new Error("Wrong period."));
            if (typeof req.body.name !== 'string' || !validator.isLength(req.body.name, { min: 1, max: 200 }))
                return callback(new Error("Period name should be 1-200 characters long."));

            var period_id = sanitize(req.body.period_id);
            var period_name = sanitize(req.body.name);

            callback(null, period_id, period_name);
        },
        function (period_id, period_name, callback) {
            Period.findById(period_id, function (err, period) {
                if (err)
                    return callback(err);
                if (period)
                    callback(null, period_name, period);
                else
                    callback(new Error("Wrong period."));
            });
        },
        function (period_name, period, callback) {
            Period.findOne({ "name": period_name, "_id": { $ne: period._id } }, function (err, res_period) {
                if (err)
                    return callback(err);
                if (res_period)
                    return callback(new Error("This period already exists."));

                period.name = period_name;
                period.save(function (err) {
                    if (err)
                        return callback(err);

                    period.name = xss(period.name);
                    callback(null, period);
                });
            });
        }
    ],
        function (err, result) {
            if (err)
                return res.send({ code: 0, resp: err.message });

            res.send({ code: 1, resp: "Period " + result.name + " updated.", result: result });
        });
});

//subjects
router.get('/subjects', function (req, res, next) {
    Subject.find({}, {}, { sort: { "name": 1 } }, function (err, subjects) {
        if (err)
            return next(err);

        SecurityHelper.xssClearNamesInArray(subjects);

        Period.find({}, {}, { sort: { "name": 1 } }, function (err, periods) {
            if (err)
                return next(err);

            SecurityHelper.xssClearNamesInArray(periods);

            res.render('admin_subjects', { subjects: subjects, periods: periods });
        });
    });
});

router.post('/subjects/add', function (req, res, next) {
    async.waterfall([
        function (callback) {
            if (typeof req.body.new_subject_name !== 'string' || !validator.isLength(req.body.new_subject_name, { min: 1, max: 200 }))
                return callback(new Error("Subject name should be 1-200 characters long"));

            if (typeof req.body.period_ids === "string")   //one selected
                req.body.period_ids = [req.body.period_ids];
            else if (!(Array.isArray(req.body.period_ids) && req.body.period_ids.length > 0 && SecurityHelper.areArrayElementsStringAndObjectId(req.body.period_ids)))
                return callback(new Error("Wrong periods."));

            var subject_name = sanitize(req.body.new_subject_name);
            Subject.findOne({ "name": subject_name }, function (err, subject) {
                if (err)
                    return callback(err);
                if (subject)
                    return callback(new Error("This subject already exists."));
                else
                    callback(null, subject_name);
            });
        },
        function (subject_name, callback) {

            var period_ids = SecurityHelper.uniqueElements(req.body.period_ids);
            
            if(!SecurityHelper.areArrayElementsStringAndObjectId(period_ids))
                return callback(new Error("Wrong periods."));

            Period.count({ "_id": { $in: period_ids } }, function (err, c) {
                if (err)
                    return next(err);

                if (c == period_ids.length)
                    callback(null, subject_name, period_ids);
                else
                    callback(new Error("Wrong periods."));
            });
        },
        function (subject_name, period_ids, callback) {
            var subject = new Subject({
                name: subject_name,
                period_ids: period_ids
            });

            subject.save(function (err, new_subject) {
                if (err)
                    return callback(err);

                Period.find({}, {}, { sort: { "name": 1 } }, function (err, all_periods) {
                    if (err)
                        return callback(err);

                    SecurityHelper.xssClearNamesInArray(all_periods);

                    var result = {
                        name: xss(new_subject.name),
                        _id: new_subject._id,
                        period_ids: new_subject.period_ids,
                        periods: all_periods
                    }
                    callback(null, result);
                })
            });
        }
    ], function (err, result) {
        if (err)
            return res.send({ code: 0, resp: err.message });

        res.send({ code: 1, resp: "Subject " + result.name + " created.", result: result });
    });
});

router.post('/subjects/delete', function (req, res, next) {
    async.waterfall([
        function (callback) {
            if (typeof req.body.subject_id !== 'string' || !MyRegex.ObjectId_regex.test(req.body.subject_id))
                return callback(new Error("Wrong subject."));

            var subject_id = sanitize(req.body.subject_id);
            Subject.findOneAndRemove({ "_id": subject_id }, function (err, subject) {
                if (err)
                    return callback(err);
                if (subject)
                    callback(null, xss(subject.name));
                else
                    callback(new Error("This subject doesn't exist."));
            });
        }
    ], function (err, result) {
        if (err)
            return res.send({ code: 0, resp: err.message });

        res.send({ code: 1, resp: "Subject " + result + " deleted." });
    })
});

router.post('/subjects/update', function (req, res, next) {
    async.waterfall([
        function (callback) {

            if (typeof req.body.subject_id !== 'string' || !MyRegex.ObjectId_regex.test(req.body.subject_id))
                return callback(new Error("Wrong subject."));

            if (typeof req.body.name !== 'string' || !validator.isLength(req.body.name, { min: 1, max: 200 }))
                return callback(new Error("Subject name should be 1-200 characters long."));

            if (typeof req.body["period_ids[]"] === "string")
                req.body["period_ids[]"] = [req.body["period_ids[]"]];
            if (!(Array.isArray(req.body["period_ids[]"]) && req.body["period_ids[]"].length > 0 && SecurityHelper.areArrayElementsStringAndObjectId(req.body["period_ids[]"])))
                return callback(new Error("Select periods."));

            var subject_id = sanitize(req.body.subject_id);
            Subject.findById(subject_id, function (err, subject) {
                if (err)
                    return callback(err);
                if (subject)
                    callback(null, subject, subject_id);
                else
                    callback(new Error("Wrong subject."));
            });
        },
        function (subject, subject_id, callback) {

            var subject_name = sanitize(req.body.name);
            Subject.findOne({ "name": subject_name, "_id": { $ne: subject_id } }, function (err, second_subject) {
                if (err)
                    return callback(err);
                if (second_subject)
                    callback(new Error("This subject already exists."));
                else {
                    subject.name = subject_name;
                    callback(null, subject);
                }
            });
        },
        function (subject, callback) {

            var period_ids = SecurityHelper.uniqueElements(req.body["period_ids[]"]);
            period_ids = sanitize(period_ids);

            Period.count({ "_id": { $in: period_ids } }, function (err, c) {
                if (err)
                    return next(err);

                if (c == period_ids.length) {
                    subject.period_ids = period_ids;
                    callback(null, subject);
                }
                else
                    callback(new Error("Wrong periods."));
            });
        },
        function (subject, callback) {

            subject.save(function (err) {
                if (err)
                    return callback(err);

                subject.name = xss(subject.name);
                var result = {
                    subject: subject
                };
                callback(null, result);
            });
        }
    ], function (err, result) {
        if (err)
            return res.send({ code: 0, resp: err.message });

        res.send({ code: 1, resp: "Subject " + result.subject.name + " updated.", result: result });
    });
});

//users
router.get('/users', function (req, res, next) {
    var curr_page;

    if (isNaN(req.query.page) || Number(req.query.page) < 0 && Number(req.query.page) > 99999999) { //nan means not a number
        curr_page = 0;
    } else {
        curr_page = Number(req.query.page);
    }

    var results_size = 15;
    var skip_val = results_size * curr_page;

    User.count({}, function (err, c) {
        if (err)
            return next(err);
        if(c<0)
            return next(new Error("Internale error"));
 
        User.find({}, { 'username': 1, 'nickname': 1, 'email': 1 }, { sort: { nickname: 1 }, skip: skip_val, limit: results_size }, function (err, users) {
            if (err)
                return next(err);

            var sum_pages = Math.ceil(c/ results_size);

            if (curr_page > sum_pages)
                curr_page = 0;

            res.render('admin_users', { users: users, curr_page: curr_page, sum_pages: sum_pages });
        });
    });
});

router.get('/users/:id', function (req, res, next) {

    if (typeof req.params.id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.id))
        return res.render('admin_user', { error_msg: "That user doesn't exist." });

    var user_id = sanitize(req.params.id);
    User.findOne({ _id: user_id }, function (err, user) {
        if (err)
            return next(err);
        
        if(user){
            Role.find({}, function (err, roles) {
                if (err)
                    return next(err);
    
                if (typeof user.facebook_id !== 'undefined')
                    user.origin = 'facebook';
                else if (typeof user.google_id !== 'undefined')
                    user.origin = 'google';
                else
                    user.origin = 'registration';
    
                user.role_name = RoleHelper.findRoleName(roles, user.role_id);
                var xss_clear_searched_user = SecurityHelper.xssClearUserForAdmin(user);
    
                res.render('admin_user', { searched_user: xss_clear_searched_user, roles: roles });
            });
        }
        else{
            res.render('admin_user', { error_msg: "That user doesn't exist." });
        }
    });
});

router.post('/users/:id', function (req, res, next) {

    if (typeof req.params.id !== 'string' || !MyRegex.ObjectId_regex.test(req.params.id))
        return res.send({ code: 0, resp: "Wrong user." });
    if (typeof req.body.role_id !== 'string' || !MyRegex.ObjectId_regex.test(req.body.role_id))
        return res.send({ code: 0, resp: "Wrong role." });
    if (typeof req.body.banned !== 'string')
        return res.send({ code: 0, resp: "Wrong banned value." });

    var user_id = sanitize(req.params.id);
    var role_id = sanitize(req.body.role_id);

    var banned_val = (req.body.banned == "true");

    User.findOne({ _id: user_id }, function (err, user) {
        if (err)
            return res.send({ code: 0, resp: "Internal error!" });
        if (!user)
            return res.send({ code: 0, resp: "User does not exist!" });

        Role.findOne({ _id: role_id }, function (err, role) {
            if (err)
                return res.send({ code: 0, resp: "Internal error!" });
            if (!role)
                return res.send({ code: 0, resp: "Role does not exist!" });

            user.role_id = role_id;
            user.banned = banned_val;
            user.save(function (err, user) {
                if (err)
                    return res.send({ code: 0, resp: "Internal error!" });

                res.send({ code: 1, resp: "User updated." });
            });
        });
    });
});

router.get('/materials', function (req, res, next) {
    res.render('admin_materials');
});

router.post('/materials', function (req, res, next) {
    var material_id = req.body.material_id;
    res.redirect('../../materials/' + material_id);
});

module.exports = router;