//passport configuration

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var configAuth = require('./social_media_auth');
var User = require('../models/user');
var sanitize = require('mongo-sanitize');
var MyAuth = require('../config/my_auth');
//serializer
passport.serializeUser(function (user, done) {
    user.id = sanitize(user.id);
    done(null, user.id);
});
//deserializer
passport.deserializeUser(function (id, done) {
    id = sanitize(id);
    User.getUserById(id, function (err, user) {
        done(err, user);
    });
});

var comp_pass = function (user, password, done) {
    user.comparePassword(password, function (err, isMatch) {
        if (err) {
            throw err;
        }
        if (isMatch)
            return done(null, user);
        else
            return done(null, false);
    });
}
//-------------------------------------------------Local strategy----------------------------------
passport.use(new LocalStrategy(
    function (username, password, done) {
        var username = sanitize(username);
        var password = sanitize(password);

        User.findOne({ username: username }, function (err, user) {
            if (err)
                return done(err);
            if (!user) {
                User.findOne({ email: username }, function (err, user) {
                    if (err)
                        return done(err);
                    if (!user) {
                        return done(null, false);
                    } else {
                        comp_pass(user, password, done);
                    }
                });
            }
            else {
                comp_pass(user, password, done);
            }
        });
    }));

//-------------------------------------------------FB strategy-------------------------------------
//facebook authentication
passport.use(new FacebookStrategy({
    // set necessary fb authorization data
    clientID: configAuth.facebookAuth.clientID,
    clientSecret: configAuth.facebookAuth.clientSecret,
    callbackURL: configAuth.facebookAuth.callbackURL
},
    // facebook will send back the token and profile
    function (token, refreshToken, profile, done) {

        // defer action (asynchronous)
        process.nextTick(function () {

            // find user in db by facebook_id
            profile.id = sanitize(profile.id);
            User.findOne({ 'facebook_id': profile.id }, function (err, user) {

                //if error then stop 
                if (err)
                    return done(err);

                // if user exists in db then log in
                if (user) {
                    if (user.banned == true)
                        return done(null, false, { message: "Your account is banned." });
                    else
                        return done(null, user);
                } else {

                    // if user doesn't exist in then create one
                    var newUser = new User({
                        facebook_id: profile.id,					//facebook id for recognize user 
                        nickname: profile.displayName,			//nick
                        first_name: profile.name.givenName,		//name
                        surname: profile.name.familyName,		//surname	
                        avatar: "default_avatar.png"
                    });

                    MyAuth.setRoleUser(newUser, function (user) {
                        user.save(function (err, user) {
                            if (err)
                                throw err;
                            return done(null, user);
                        });
                    });

                }

            });
        });

    }));


//-------------------------------------------------Google strategy---------------------------------
//google authentication
// Use the GoogleStrategy within Passport.
//   Strategies in passport require a `verify` function, which accept
//   credentials (in this case, a token, tokenSecret, and Google profile), and
//   invoke a callback with a user object.
passport.use(new GoogleStrategy({
    clientID: configAuth.googleAuth.clientID,
    clientSecret: configAuth.googleAuth.clientSecret,
    callbackURL: configAuth.googleAuth.callbackURL,
},
    function (token, refreshToken, profile, done) {

        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function () {

            // try to find the user based on their google id

            profile.id = sanitize(profile.id);
            User.findOne({ 'google_id': profile.id }, function (err, user) {
                if (err)
                    return done(err);

                if (user) {
                    if (user.banned == true)
                        return done(null, false, { message: "Your account is banned." });
                    else
                        // if a user is found, log them in
                        return done(null, user);
                } else {

                    // if user doesn't exist in then create one
                    var newUser = new User({
                        google_id: profile.id,					//google id for recognize user 
                        nickname: profile.displayName,			//nick
                        first_name: profile.name.givenName,		//name
                        surname: profile.name.familyName,		//surname	
                        avatar: "default_avatar.png"
                    });
                    //put object to database
                    MyAuth.setRoleUser(newUser, function (user) {

                        user.save(function (err, user) {       //createUser
                            if (err)
                                throw err;
                            return done(null, user);
                        });
                    });
                }
            });
        });

    }));

// //helepr function that gets email address from array
// function get_email_addr(emails_arr) {
//     if (typeof emails_arr !== 'undefined' && emails_arr !== null && emails_arr.isArray && emails_arr.length > 0) {
//         return emails_arr[0].value;
//     }
//     else {
//         return undefined;
//     }
// }

module.exports = passport;