//brute force prevention

var EnvironmentConfig = require("../config/environment");
var ExpressBrute = require('express-brute');
var BruteForceSchema = require('express-brute-mongoose/dist/schema');
var MongooseStore = require('express-brute-mongoose');
var mongoose = require('mongoose');
var BruteForceModel = mongoose.model('bruteforce', BruteForceSchema);
var moment = require('moment');

var store;
if (EnvironmentConfig.environment == 'development') {
    store = new ExpressBrute.MemoryStore(); // stores state locally, don't use this in production 
} else {
    // stores state with mongodb but also could be done with memcached 
    store = new MongooseStore(BruteForceModel);
}

//var bruteforce = new ExpressBrute(store);

var failCallback = function (req, res, next, nextValidRequestDate) {
    var data = {
        code: 2,
        resp: "You've made too many attempts in a short period of time, please try again " + moment(nextValidRequestDate).fromNow()
    };
    res.send(data);
};

var handleStoreError = function (error) {
    log.error(error); // log this error so we can figure out what went wrong 
    // cause node to exit, hopefully restarting the process fixes the problem 
    throw {
        message: error.message,
        parent: error.parent
    };
}

// Start slowing requests after 5 failed attempts to do something for the same user 
var userBruteforce = new ExpressBrute(store, {
    freeRetries: 5,		//how many tries before blocking user for certain amount of time
    minWait: 60 * 1000, // 1 minute
    maxWait: 60 * 60 * 1000, // 1 hour
    failCallback: failCallback,
    handleStoreError: handleStoreError
});

// No more than 1000 login attempts per day per IP 
var globalBruteforce = new ExpressBrute(store, {
    freeRetries: 1000,
    attachResetToRequest: false,
    refreshTimeoutOnRequest: false,
    minWait: 25 * 60 * 60 * 1000, // 1 day 1 hour (should never reach this wait time) 
    maxWait: 25 * 60 * 60 * 1000, // 1 day 1 hour (should never reach this wait time) 
    lifetime: 24 * 60 * 60, // 1 day (seconds not milliseconds) 
    failCallback: failCallback,
    handleStoreError: handleStoreError
});

var failCallbackContact = function (req, res, next, nextValidRequestDate) {
    var data = {
        code: 2,
        resp: "You've made too many attempts in a short period of time, please try again " + moment(nextValidRequestDate).fromNow()
    };

    res.send(data);
};

var contactBruteForce = new ExpressBrute(store, {
    freeRetries: 15,
    attachResetToRequest: false,
    refreshTimeoutOnRequest: false,
    minWait: 25 * 60 * 60 * 1000, // 1 day 1 hour (should never reach this wait time) 
    maxWait: 25 * 60 * 60 * 1000, // 1 day 1 hour (should never reach this wait time) 
    lifetime: 24 * 60 * 60, // 1 day (seconds not milliseconds) 
    failCallback: failCallbackContact,
    handleStoreError: handleStoreError
});

//app.set('trust proxy', 1); // Don't set to "true", it's not secure. Make sure it matches your environment 

module.exports = {
    userBruteforce: userBruteforce,
    globalBruteforce: globalBruteforce,
    contactBruteForce: contactBruteForce
}