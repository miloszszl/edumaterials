//methods for authentication
var Role = require('../models/role');
var xss = require('xss');
var sanitize = require('mongo-sanitize');

var checkRole = function (role_name) {
	return function (req, res, next) {
		if (typeof req.user !== 'undefined' && req.user && req.user.role_name == role_name) {
			next();
		} else {
			var error = new Error("Acces denied.");
			error.status = 401;
			next(error);
		}
	};
};

var findUserRole = function (req, res, next) {
	if (typeof req.user !== 'undefined' && req.user && typeof req.user.role_name === 'undefined') {
		Role.findOne({ "_id": sanitize(req.user.role_id) }, function (err, role) {
			if (err || !role) {
				next(err);
			}
			else {
				req.user.role_name = xss(role.name);
				next();
			}
		});
	} else {
		next();
	}
}

function ensureNotAuthenticated(req, res, next) {
	if (!req.isAuthenticated()) {
		next();
	} else {
		req.flash('error_msg', "You must be logged out to perform this operation.");
		res.redirect('/start');
	}
}

function checkIfIsOwner(req, res, next) {
	if (req.user && typeof req.user !== 'undefined' && req.user._id && typeof req.user._id !== 'undefined' && req.params.user_id && typeof req.params.user_id === 'string') {
		if (req.user._id.equals(req.params.user_id)) 
			req.user.is_owner = true;
		else 
			req.user.is_owner = false;
	} 

	next();
}

function checkIfUserIsAuthenticated(req) {
	if (req.isAuthenticated() && typeof req.user !== 'undefined' && req.user.banned == false)
		return true;
	else
		return false;
}

function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) {
		if (req.user.banned == true) {
			req.logout();
			req.flash('error_msg', 'Your account is banned.');
			res.redirect('/start');
		} else
			next();
	} else {
		req.flash('error_msg', 'Please log in to do this action.');
		res.redirect('/start');
	}
}

function ensureAuthenticatedAjax(req, res, next) {

	if (req.isAuthenticated()) {
		if (req.user.banned == true) {
			req.logout();
			res.send({ code: 0, resp: 'Your account is banned.' });
		} else
			next();
	} else
		res.send({ code: 0, resp: 'Please log in.' });
}

var setRoleUser = function (user, cb) {
	Role.findOne({ name: 'user' }, { "_id": 1 }, function (err, role_result) {
		if (err)
			return next(err);
		else if (role_result) {
			user.role_id = role_result._id;
		}
		cb(user);
	});
}

module.exports = {
	checkRole: checkRole,
	ensureAuthenticated: ensureAuthenticated,
	ensureAuthenticatedAjax: ensureAuthenticatedAjax,
	ensureNotAuthenticated: ensureNotAuthenticated,
	setRoleUser: setRoleUser,
	findUserRole: findUserRole,
	checkIfIsOwner: checkIfIsOwner,
	checkIfUserIsAuthenticated: checkIfUserIsAuthenticated
}