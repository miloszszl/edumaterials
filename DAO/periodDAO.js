var MetadataParser=require('../helpers/MetadataParser');
var Period=require('../models/period');
var PeriodHelper=require('../helpers/PeriodHelper');

class PeriodDAO{
    static findByNameRegexCaseInsensitive(period_name,full_metadata,callback){
        var internal_error="Internal error!";
        var too_many_err="Too many matches for period! Please specify it.";
        var no_matches="No matches for period!";
        
        var metadata_object=MetadataParser.createRegexObject(period_name);
        full_metadata.period={};

        Period.find({name:{$regex:new RegExp(metadata_object.exact_name, "i")}},function(err,results){
          if(err)
            return callback(new Error(internal_error));
      
          if(results.length<1){
      
            Period.find({name:{$regex:new RegExp(metadata_object.name_beginning, "i")}},function(err,results){
              if(err)
                return callback(new Error(internal_error));
      
              if(results.length==1){
                full_metadata.period.name=results[0].name;
                full_metadata.period._id=results[0]._id;
                return callback(null,full_metadata);
              }
              else if(results.length<1){
      
                Period.find({name:{$regex:new RegExp(metadata_object.name_acronym, "i")}},function(err,results){
                  if(err)
                    return callback(new Error(internal_error));
          
                  if(results.length==1){
                    full_metadata.period.name=results[0].name;
                    full_metadata.period._id=results[0]._id;
                    return callback(null,full_metadata);
                  }
                  else if(results.length<1)
                    return callback(new Error(no_matches));
                  else
                    return callback(new Error(too_many_err));
                });
              }
              else
                return callback(new Error(too_many_err));    
            });
          }else if(results.length==1){
            full_metadata.period.name=results[0].name;
            full_metadata.period._id=results[0]._id;
            return callback(null,full_metadata);
          }else
            return callback(new Error(too_many_err));
        });
    }

    static findManyByNameRegexCaseInsensitive(metadata_object,callback){
      Period.find({$or:[{name:{$regex:new RegExp(metadata_object.exact_name, "i")}},
      {name:{$regex:new RegExp(metadata_object.name_beginning, "i")}},
      {name:{$regex:new RegExp(metadata_object.name_acronym, "i")}}]},{},function(err,results){ //,{limit:7}

        if(err)
          return callback(new Error("Internal error!"));

          var results_arr=PeriodHelper.preparePeriodsAutocomplete(results);
          callback(null,results_arr);
      });
    }

    static findIdByNameCaseInsensitive(period_name,callback){
      var metadata_object=MetadataParser.createRegexObject(period_name);

      Period.find({name:{$regex:new RegExp(metadata_object.exact_name, "i")}},{"_id":1},function(err,results){
        if(err)
          return callback(new Error());
    
        if(results.length<1){
    
          Period.find({name:{$regex:new RegExp(metadata_object.name_beginning, "i")}},function(err,results){
            if(err)
              return callback(new Error());
    
            if(results.length==1){
              return callback(null,results[0]._id);
            }
            else if(results.length<1){
    
              Period.find({name:{$regex:new RegExp(metadata_object.name_acronym, "i")}},function(err,results){
                if(err)
                  return callback(new Error());
        
                if(results.length==1){
                  return callback(null,results[0]._id);
                }
                else if(results.length<1)
                  return callback(new Error());
                else
                  return callback(new Error());
              });
            }
            else
              return callback(new Error());    
          });
        }else if(results.length==1){
          return callback(null,results[0]._id);
        }else
          return callback(new Error());
      });
    }
}

module.exports=PeriodDAO