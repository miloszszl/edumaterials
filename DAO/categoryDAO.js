var MetadataParser=require('../helpers/MetadataParser');
var Category=require('../models/category');
var CategoryHelper=require('../helpers/CategoryHelper');

class CategoryDAO{
    static findByNameRegexCaseInsensitive(category_name,full_metadata,callback){
        var internal_error="Internal error!";
        var too_many_err="Too many matches for category! Please specify it.";
        var no_matches="No matches for category!";
        
        var metadata_object=MetadataParser.createRegexObject(category_name);
        full_metadata.category={};

        Category.find({name:{$regex:new RegExp(metadata_object.exact_name, "i")},period_ids:full_metadata.period._id},function(err,results){
          if(err)
            return callback(new Error(internal_error));
      
          if(results.length<1){
      
            Category.find({name:{$regex:new RegExp(metadata_object.name_beginning, "i")},period_ids:full_metadata.period._id},function(err,results){
              if(err)
                return callback(new Error(internal_error));
      
              if(results.length==1){
                full_metadata.category.name=results[0].name;
                full_metadata.category._id=results[0]._id;
                return callback(null,full_metadata);
              }
              else if(results.length<1){
      
                Category.find({name:{$regex:new RegExp(metadata_object.name_acronym, "i")},period_ids:full_metadata.period._id},function(err,results){
                  if(err)
                    return callback(new Error(internal_error));
          
                  if(results.length==1){
                    full_metadata.category.name=results[0].name;
                    full_metadata.category._id=results[0]._id;
                    return callback(null,full_metadata);
                  }
                  else if(results.length<1)
                    return callback(new Error(no_matches));
                  else
                    return callback(new Error(too_many_err));
                });
              }
              else
                return callback(new Error(too_many_err));    
            });
          }else if(results.length==1){
            full_metadata.category.name=results[0].name;
            full_metadata.category._id=results[0]._id;
            return callback(null,full_metadata);
          }else
            return callback(new Error(too_many_err));
        });
    }

    static findManyByNameRegexCaseInsensitive(period_id,metadata_object,callback){
      Category.find({$or:[{name:{$regex:new RegExp(metadata_object.exact_name, "i")}},
      {name:{$regex:new RegExp(metadata_object.name_beginning, "i")}},
      {name:{$regex:new RegExp(metadata_object.name_acronym, "i")}}],period_ids:period_id},{},function(err,results){ //,{limit:7}
        if(err)
          return callback(new Error("Internal error!"));

          var results_arr=CategoryHelper.prepareCategoriesAutocomplete(results);
          callback(null,results_arr);
      });
    }
}

module.exports=CategoryDAO