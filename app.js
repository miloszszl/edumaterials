var express = require('express');
var session = require('express-session');
var expressValidator = require('express-validator');
var mongoose = require('mongoose');
var flash = require('connect-flash');
var passport = require('passport');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var configMongoDB = require('./config/database');
var db = mongoose.connection;
var configEnv = require('./config/environment');
var helmet = require('helmet');
var fs = require('fs');
var https = require('https');
var index = require('./routes/index');
var users = require('./routes/users');
var material = require('./routes/materials');
var admin_panel = require('./routes/admin_panel');
var MyAuth = require('./config/my_auth');
var favourite = require('./routes/favourite');
var csurf = require('csurf')
//csrf
//var csrfProtection = csrf({ cookie: true });
//connect to databse
mongoose.Promise = global.Promise;
mongoose.connect(configMongoDB.url, { useMongoClient: true });

//initialize app
var app = express();
app.use(helmet());
app.use(helmet.noCache());

//view enging
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Express Validator
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        var namespace = param.split('.')
            , root = namespace.shift()
            , formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

app.use(cookieParser());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'learning_materials')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'avatars')));

// Express Session
var session_secret = 'd23dj8s0afm,0^#Rfun92*(HP#d320(';
app.use(session({
    secret: session_secret,
    saveUninitialized: true,
    resave: true,
    cookie: { secure: true } 
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// Connect Flash
app.use(flash());

// Global Vars
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.admin_error_msg = req.flash('admin_error_msg');
    res.locals.admin_success_msg = req.flash('admin_success_msg');
    res.locals.user = req.user || null;
    next();
});

app.use(MyAuth.findUserRole);
app.use('/', index);
app.use('/users', users);
app.use('/materials', material);
app.use('/admin_panel', MyAuth.ensureAuthenticated, MyAuth.checkRole(configEnv.admin_role), admin_panel);
app.use('/favourite', MyAuth.ensureAuthenticatedAjax, favourite);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('We are looking for your page but we can\'t find it...');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    // res.locals.message = err.message;
    // res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page

    var err_code = err.status || 500;
    var err_message = err.message;
    res.status(err_code);
    if (err_code == 500) {
        err_message = err//"Server failed. Please try later."
    }
    res.render('error', { err_code: err_code, err_message: err_message });
});

// Set Port
app.set('port', (process.env.PORT || 3000));

// app.listen(app.get('port'), function(){
// 	console.log('Server started on port '+app.get('port'));
// });

https.createServer({
    key: fs.readFileSync('ssl/key.pem'),
    cert: fs.readFileSync('ssl/cert.pem'),
    passphrase: "r+z;(]#CT[8]YZbW"
}, app).listen(app.get('port'), function () {
    console.log('Server started on port ' + app.get('port'));
});

module.exports = app
