var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var materialSchema = new Schema({
  name: {
    type:String,
    index:true,
    sparse:true
  },
  extension:String,
  relative_server_path:String,
  upload_date: { type: Date, default: Date.now },
  owner_id:Schema.Types.ObjectId,
  size:Number,    //in bytes
  type:String,
  num_of_downloads:{
    type:Number,
    default:0,
    min:0
  },
  num_of_likes:{
    type:Number,
    default:0,
    min:0
  },
  comments:[{
    text:String,
    author_id:mongoose.Schema.Types.ObjectId,
    publish_date:{
      type:Date,
      default:Date.now
    }
  }],
  description:String,
  period_id:Schema.Types.ObjectId,
  subject_id:Schema.Types.ObjectId,
  category_id:Schema.Types.ObjectId
});

//create model
var Material = mongoose.model('Material', materialSchema);

// expose model
module.exports = Material;
