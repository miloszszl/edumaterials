var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var subjectSchema=new Schema({
    name:{
        type:String,
        index:true
    },
    period_ids:[Schema.Types.ObjectId]
});

var Subject=mongoose.model('Subject',subjectSchema);

module.exports=Subject;