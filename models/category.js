var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var categorySchema=new Schema({
    name:{
        type:String,
        index:true
    },
    period_ids:[Schema.Types.ObjectId]
});

var Category=mongoose.model('Category',categorySchema);

module.exports=Category;