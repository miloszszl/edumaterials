var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var bcrypt=require('bcryptjs');
var Role=require('./role')

var UserSchema=new Schema({
    username:{
        type:String,
        index:true,
        unique: true,
        sparse:true
    },
    password:{
        type:String
    },
    email:{
        type:String,
        index:true,
        unique:true,
        sparse:true
    },
    nickname:{
        type:String
    },
    first_name:{
        type:String
    },
    surname:{
        type:String
    },
    facebook_id:{
        type:String,
    },
    google_id:{
        type:String,
    },
    banned:{
        type:Boolean,
        default:false
    },
    role_id:Schema.Types.ObjectId,
    free_disk_space:{
        type:Number,
        min:0,
        default:5120 // stre data in MB. 5120MB=5GB 
    },
    show_favourites:{
        type:Boolean,
        default:true
    },
    favourite_materials_ids:[Schema.Types.ObjectId],
    liked_materials_ids:[Schema.Types.ObjectId],
    avatar:String,
    resetPasswordToken: String,
    resetPasswordExpires: Date
});


UserSchema.pre('save', function(next) {
    var user = this;
    var SALT_FACTOR = 10;
  
    if (!user.isModified('password')) {
        return next();
    }
  
    bcrypt.genSalt(SALT_FACTOR, function(err, salt) {
      if (err) return next(err);
  
      bcrypt.hash(user.password, salt,function(err, hash) {
        if (err) return next(err);
        user.password = hash;
        next();
      });
    });
});

UserSchema.methods.comparePassword=function(candidatePassword, cb) {
    
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err){
            return cb(err);
        } 
        cb(null, isMatch);
    });
};

var User=mongoose.model('User',UserSchema);

module.exports=User;

///check if used anywhere
module.exports.createUser=function(newUser,callback){
    bcrypt.genSalt(10, function(err, salt) {
	    bcrypt.hash(newUser.password, salt, function(err, hash) {
            newUser.password = hash;
	        newUser.save(callback);
	    });
    });
}

module.exports.getUserByUsername=function(username,callback){
    var query={username:username};
    User.findOne(query,callback);
}

module.exports.getUserById=function(id,callback){
    User.findById(id,callback);
}

// module.exports.comparePassword=function(candidatePassword,hash,callback){
//     bcrypt.compare(candidatePassword,hash,function(err,isMatch){
//         if(err) throw err;
//         callback(null,isMatch);
//     });
// }