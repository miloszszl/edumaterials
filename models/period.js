var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var periodSchema = new Schema({
    name:{
        type:String,
        index:true
    }
});

//create model
var Period = mongoose.model('Period', periodSchema);

// expose model
module.exports = Period;
